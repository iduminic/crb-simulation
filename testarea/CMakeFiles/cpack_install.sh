#!/usr/bin/bash
#
# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Script used for installing a project for CPack. Hiding any build errors
# from the package generator.
#

# Turn off error reporting:
set +e

# Install the project:
DESTDIR=${DESTDIR}/usr
/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.18.3/Linux-x86_64/bin/cmake --install /afs/cern.ch/user/i/iduminic/ncb/Software/offline/MCCosmics/testarea

# Remove the .dbg files from being packaged, in case we are in RelWithDebInfo
# build mode:
/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.18.3/Linux-x86_64/bin/cmake -E remove -f ${DESTDIR}/Project/0.0.1/InstallArea/x86_64-centos7-gcc8-opt/bin/*.dbg
/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.18.3/Linux-x86_64/bin/cmake -E remove -f ${DESTDIR}/Project/0.0.1/InstallArea/x86_64-centos7-gcc8-opt/lib/*.dbg

# Exit gracefully:
exit 0

import ROOT
from ROOT import gPad

def read_tree(input_file, tree_name):
    c1 = ROOT.TCanvas("c1","c1",700,500)
    c1.Draw()   
   
    file = ROOT.TFile(input_file)
    if file.IsOpen():
    	print("File opened succesfully\n")
   	def plot_var(container, plot_filename):	
		tree = file.Get(tree_name)
		tree.Draw(container)
		gPad.Modified()
		gPad.Update()
		c1.SaveAs(plot_filename)
		
	plot_var("MuonSpectrometerTrackParticlesAuxDyn.rpcHitTime", "rpcHitTime.pdf")
	plot_var("MuonSpectrometerTrackParticlesAuxDyn.rpcHitPositionX", "rpcHitPositionX.pdf")
	plot_var("MuonSpectrometerTrackParticlesAuxDyn.rpcHitPositionY", "rpcHitPositionY.pdf")
	plot_var("MuonSpectrometerTrackParticlesAuxDyn.rpcHitPositionZ", "rpcHitPositionZ.pdf")	
	
if __name__=="__main__":
	read_tree("AOD.pool.root", "CollectionTree;1")			


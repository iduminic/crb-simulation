generation_dir=generation

[ -d $generation_dir ] || mkdir $generation_dir
cd $generation_dir

Sim_tf.py  \
--preInclude 'EVNTtoHITS:SimulationJobOptions/preInclude.Cosmics.py' \
--simulator 'FullG4' \
--truthStrategy 'MC15aPlus' \
--outputHITSFile "cosmics.HITS.pool.root" \
--maxEvents '1000' \
--randomSeed '1234' \
--DataRunNumber '284500' \
--CosmicFilterVolume 'NONE' \
--CosmicFilterVolume2 'NONE' \
--geometryVersion 'ATLAS-R3S-2021-01-00-00_VALIDATION' \
--conditionsTag 'OFLCOND-MC16-SDR-25' \
--physicsList 'FTFP_BERT' \
--postInclude 'default:RecJobTransforms/UseFrontier.py' \
--CosmicPtSlice 'NONE' \
--outputEVNT_TRFile "cosmics.TR.pool.root" \
--beamType 'cosmics'

cd ../

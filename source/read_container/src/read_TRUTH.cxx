// read_EVNT includes
#include "read_TRUTH.h"

read_TRUTH::read_TRUTH( const std::string& name, ISvcLocator* pSvcLocator ) 
//    : AthAnalysisAlgorithm( name, pSvcLocator ),
  : AthAlgorithm( name, pSvcLocator ),
      m_outputFile("read_TRUTH.root")
{

  //declareProperty( "MC_collection", m_mcCollection ); //example property declaration
  m_mcCollection = name;
  declareProperty( "outputFile", m_outputFile );

}


read_TRUTH::~read_TRUTH() {}


StatusCode read_TRUTH::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << " MC collection=" << m_mcCollection);

  m_tree = new TTree(m_mcCollection.data(),"");

  m_tree->Branch("run_number",&m_run_number,"run_number/i");
  m_tree->Branch("event_number",&m_event_number,"event_number/i");

  m_tree->Branch("n", &m_n, "m_n/I");
  m_tree->Branch("pdg_id","vector<int>",&m_pdg_id);
  m_tree->Branch("status","vector<int>",&m_status);
  m_tree->Branch("eventWeights","vector<double>",&m_weights);

  m_tree->Branch("momentum_px","vector<float>", &m_momentum_px);
  m_tree->Branch("momentum_py","vector<float>", &m_momentum_py);
  m_tree->Branch("momentum_pz","vector<float>", &m_momentum_pz);
  m_tree->Branch("momentum_e","vector<float>",&m_momentum_e);

  m_tree->Branch("vertex_point3d_x","vector<float>",&m_vertex_point3d_x);
  m_tree->Branch("vertex_point3d_y","vector<float>",&m_vertex_point3d_y);
  m_tree->Branch("vertex_point3d_z","vector<float>",&m_vertex_point3d_z);

  m_tree->Branch("vertex_position_x","vector<float>",&m_vertex_position_x);
  m_tree->Branch("vertex_position_y","vector<float>",&m_vertex_position_y);
  m_tree->Branch("vertex_position_z","vector<float>",&m_vertex_position_z);
  m_tree->Branch("vertex_position_t","vector<float>",&m_vertex_position_t);
  m_tree->Branch("vertex_position_theta","vector<float>",&m_vertex_position_theta);
  m_tree->Branch("vertex_position_phi","vector<float>",&m_vertex_position_phi);
  m_tree->Branch("vertex_position_eta","vector<float>",&m_vertex_position_eta);

  return StatusCode::SUCCESS;
}

StatusCode read_TRUTH::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  TFile* file=TFile::Open(m_outputFile.data(),"update");
 
  m_tree->Write();

  file->Close();

  return StatusCode::SUCCESS;
}

StatusCode read_TRUTH::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  const EventInfo* eventInfo = 0;
  if (evtStore()->retrieve(eventInfo, "McEventInfo").isFailure()) 
    ATH_MSG_INFO ("Failed to retrieve the MCEventInfo");
  else {
    ATH_MSG_DEBUG ( "  event_ID run_number=" << eventInfo->event_ID()->run_number() );
    ATH_MSG_DEBUG ( "           event_number=" << eventInfo->event_ID()->event_number() );
    ATH_MSG_DEBUG ( "           lumi_block=" << eventInfo->event_ID()->lumi_block() );
    ATH_MSG_DEBUG ( "           bcid=" << eventInfo->event_ID()->bunch_crossing_id() );
    ATH_MSG_DEBUG ( "           detector_mask=" << eventInfo->event_ID()->detector_mask() );  

    ATH_MSG_DEBUG ( " event_type typeToString=" << eventInfo->event_type()->typeToString() );
    ATH_MSG_DEBUG ( "            get_detdescr_tags=" << eventInfo->event_type()->get_detdescr_tags() );
    ATH_MSG_DEBUG ( "            mc_channel_number=" << eventInfo->event_type()->mc_channel_number() );
    ATH_MSG_DEBUG ( "            mc_event_number=" << eventInfo->event_type()->mc_event_number() );
    ATH_MSG_DEBUG ( "            mc_event_weight=" << eventInfo->event_type()->mc_event_weight() );

    ATH_MSG_DEBUG ( "trigger_info statusElement=" << eventInfo->trigger_info()->statusElement() );
    ATH_MSG_DEBUG ( "             extendedLevel1ID=" << eventInfo->trigger_info()->extendedLevel1ID() );
    ATH_MSG_DEBUG ( "             level1TriggerType=" << eventInfo->trigger_info()->level1TriggerType() );

    ATH_MSG_DEBUG ( "  actualInteractionsPerCrossing=" << eventInfo->actualInteractionsPerCrossing());  
  }

  const DataHandle<McEventCollection> mcCollptr;

  if ( evtStore()->retrieve(mcCollptr, m_mcCollection).isFailure() ) { // evgen files contain McEventCollection with GEN_EVENT key
    ATH_MSG_INFO ( "cannot retrieve McEventCollection: " << m_mcCollection);
    return StatusCode::SUCCESS;
  }
 
  m_run_number = eventInfo->event_ID()->run_number();
  m_event_number = eventInfo->event_ID()->event_number();

  m_pdg_id.clear();
  m_status.clear();
  m_weights.clear();

  m_momentum_px.clear();
  m_momentum_py.clear();
  m_momentum_pz.clear();
  m_momentum_e.clear();  

  m_vertex_point3d_x.clear();
  m_vertex_point3d_y.clear();
  m_vertex_point3d_z.clear();

  m_vertex_position_x.clear();
  m_vertex_position_y.clear();
  m_vertex_position_z.clear();
  m_vertex_position_t.clear();
  m_vertex_position_theta.clear();
  m_vertex_position_phi.clear();
  m_vertex_position_eta.clear();

  McEventCollection::const_iterator itr;
  if (mcCollptr->size() != 1) {
    ATH_MSG_INFO ("ncCollPtr expected to have dimension of 1");
    return StatusCode::SUCCESS;
  }
  
  itr = mcCollptr->begin(); 
  ATH_MSG_DEBUG( "  event_number="<<(*itr)->event_number());;

  HepMC::GenEvent::particle_const_iterator itrPart;
  m_n = (*itr)->particles_size(); 
  ATH_MSG_DEBUG ( "m_n=" << m_n );
  for (itrPart = (*itr)->particles_begin(); itrPart!=(*itr)->particles_end(); ++itrPart ) {
    // GenParticle does not have a time value.
    HepMC::GenParticle *part=*itrPart;
    //afs/cern.ch/sw/lcg/external/HepMC/2.03.06/slc3_ia32_gcc344/include/HepMC/GenParticle.h
    ///afs/cern.ch/sw/lcg/external/HepMC/2.03.06/src/HepMC-2.03.06/src/
    ATH_MSG_DEBUG("pdg_id="<<part->pdg_id() 
                     << " status=" << part->status()
                     << " px=" << part->momentum().px()<< " "
                     << " py=" << part->momentum().py()<< " "
                     << " pz=" << part->momentum().pz()<< " "  
                     << " e(total energy)="  << part->momentum().e() << " MeV" );
    //ATH_MSG_DEBUG("parent_event:");
    //part->parent_event()->print(); 
    //ATH_MSG_INFO("production_vertex:");
    //ATH_MSG_INFO("  #in particles="<<part->production_vertex()->particles_in_size());
    //ATH_MSG_INFO("  #out particles="<<part->production_vertex()->particles_out_size());
    //ATH_MSG_INFO("  (x,y,z)="<<part->production_vertex()->point3d().x()<<", "<<part->production_vertex()->point3d().y()<<", "<<part->production_vertex()->point3d().z()); 
    //part->production_vertex()->print();
    //part->print();

    m_pdg_id.push_back(part->pdg_id());
    m_status.push_back(part->status());

    m_momentum_px.push_back(part->momentum().px());
    m_momentum_py.push_back(part->momentum().py());
    m_momentum_pz.push_back(part->momentum().pz());
    m_momentum_e.push_back(part->momentum().e());

    m_vertex_point3d_x.push_back(part->production_vertex()->point3d().x());
    m_vertex_point3d_y.push_back(part->production_vertex()->point3d().y()); 
    m_vertex_point3d_z.push_back(part->production_vertex()->point3d().z());
 
    m_vertex_position_x.push_back(part->production_vertex()->position().x());
    m_vertex_position_y.push_back(part->production_vertex()->position().y());
    m_vertex_position_z.push_back(part->production_vertex()->position().z());
    m_vertex_position_t.push_back(part->production_vertex()->position().t());
    m_vertex_position_theta.push_back(part->production_vertex()->position().theta());
    m_vertex_position_phi.push_back(part->production_vertex()->position().phi());
    m_vertex_position_eta.push_back(part->production_vertex()->position().eta());
  }

  for (double w : (*itr)->weights() )
    m_weights.push_back(w);

  m_tree->Fill();
  
  return StatusCode::SUCCESS;
}

StatusCode read_TRUTH::beginInputFile() {  
  //example of metadata retrieval:
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );

  return StatusCode::SUCCESS;
}



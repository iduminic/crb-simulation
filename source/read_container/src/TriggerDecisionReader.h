#ifndef TRIGGER_DEC_READER_H
#define TRIGGER_DEC_REDER_H

#include <vector>
#include <string>
using namespace std;

#include <TFile.h>
#include <TTree.h>

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" //included under assumption you'll want to use some tools! Remove if you don't!

#include "TrigDecisionTool/TrigDecisionTool.h"
#include "xAODEventInfo/EventInfo.h"
#include "TROOT.h"

class TriggerDecisionReader : public AthAlgorithm {

  public:
             TriggerDecisionReader( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~TriggerDecisionReader();

    virtual StatusCode initialize();
    virtual StatusCode execute();
    virtual StatusCode finalize();

  private:

   TFile* m_file;
   TTree* m_tree;
   string m_outputFile;

   unsigned int m_runNumber;
   unsigned int m_eventNumber;
   unsigned int m_bcid;

   ToolHandle<Trig::TrigDecisionTool> m_trigTool;
   vector<string> m_trigItem_names;
   vector<int> m_trigItem_isPassed;
};


#endif

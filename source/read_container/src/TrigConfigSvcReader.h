#ifndef TRIG_CONFIG_SVC_READER_H
#define TRIG_CONFIG_SVC_READER_H

#include <vector>
#include <string>
#include <fstream>
using namespace std;

#include <TFile.h>
#include <TTree.h>

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ServiceHandle.h"
//#include "TrigConfInterfaces/ITrigConfigSvc.h"
//#include "TrigConfInterfaces/ILVL1ConfigSvc.h"
//#include "TrigConfInterfaces/IHLTConfigSvc.h"
#include "TrigConfInterfaces/ITrigConfigSvc.h" 
#include "TrigConfL1Data/CTPConfig.h"
#include "TrigConfL1Data/BunchGroup.h"
#include "TrigConfL1Data/BunchGroupSet.h"
#include "TrigConfL1Data/ThresholdConfig.h"
#include "TrigConfL1Data/TriggerItem.h"
#include "TrigConfL1Data/Muctpi.h"
#include "TrigConfHLTData/HLTChainList.h"
#include "TrigConfHLTData/HLTSequenceList.h"

class TrigConfigSvcReader : public AthAlgorithm {

  public:
             TrigConfigSvcReader( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~TrigConfigSvcReader();

    virtual StatusCode initialize();
    virtual StatusCode execute();
    virtual StatusCode finalize();

  private:

   //ServiceHandle< TrigConf::ITrigConfigSvc > m_trigconf;
   //ServiceHandle< TrigConf::ILVL1ConfigSvc > m_trigconf;
   //ServiceHandle< TrigConf::IHLTConfigSvc> m_trigconf;
   ServiceHandle< TrigConf::ITrigConfigSvc> m_trigconf;

   TFile* m_file;
   TTree* m_tree;
   string m_outputFile;

};

#endif

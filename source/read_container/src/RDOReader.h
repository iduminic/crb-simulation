#ifndef READ_CONTAINER_PIXELRDOREADER_H
#define READ_CONTAINER_PIXELRDOREADER_H 

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" //included under assumption you'll want to use some tools! Remove if you don't!

#include "EventInfo/EventInfo.h"
#include "EventInfo/EventID.h"
#include "InDetRawData/PixelRDO_Container.h"
#include "StoreGate/DataHandle.h"
#include "InDetIdentifier/PixelID.h"
#include "InDetEventAthenaPool/SCT_RawDataContainer_p3.h"
#include "InDetBCM_RawData/BCM_RDO_Container.h"

#include <TTree.h>
#include <TFile.h>
#include <TROOT.h>

#include <string>
#include <fstream>
#include <vector>
using namespace std;
#include <stdlib.h>

class RDOReader: public ::AthAlgorithm { 
 public: 
           RDOReader( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~RDOReader(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();
 
 private: 

  bool m_doBCM;
  
  unsigned int m_BCM_RDO_Container_nCollections;

  vector<unsigned int> m_BCM_RDO_Collection_nBCM_RDOs;
  vector<unsigned int> m_BCM_RDO_Collection_channel;

  vector<int> m_BCM_RawData_word1;
  vector<int> m_BCM_RawData_word2;
  vector<int> m_BCM_RawData_channel;
  vector<int> m_BCM_RawData_pulse1Position;
  vector<int> m_BCM_RawData_pulse1Width;
  vector<int> m_BCM_RawData_pulse2Position;
  vector<int> m_BCM_RawData_pulse2Width;
  vector<int> m_BCM_RawData_LVL1A;
  vector<int> m_BCM_RawData_BCID;
  vector<int> m_BCM_RawData_LVL1ID;
  vector<int> m_BCM_RawData_Error;

  bool m_doPixel;

  const PixelID* m_pixelID;

  unsigned int m_run_number;
  unsigned int m_event_number;

  vector<int> m_pixelRDORawData_ToT;
  vector<int> m_pixelRDORawData_BCID;
  vector<int> m_pixelRDORawData_LVL1A;
  vector<int> m_pixelRDORawData_LVL1ID;
  
  vector<int> m_pixelRDORawData_is_barrel;
  vector<int> m_pixelRDORawData_is_dbm;
  vector<int> m_pixelRDORawData_is_blayer;
  vector<int> m_pixelRDORawData_layer_disk;
  vector<int> m_pixelRDORawData_barrel_ec;
  vector<int> m_pixelRDORawData_phi_module;
  vector<int> m_pixelRDORawData_eta_module;
  vector<int> m_pixelRDORawData_phi_index;
  vector<int> m_pixelRDORawData_eta_index;
  vector<unsigned int long> m_pixelRDORawData_id;

  vector<int> m_SCT_RDORawData_timeBin;

  TFile* m_file;
  TTree* m_tree;

}; 

#endif //> !READ_CONTAINER_PIXELRDOREADER_H

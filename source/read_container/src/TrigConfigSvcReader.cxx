#include "TrigConfigSvcReader.h"

TrigConfigSvcReader::TrigConfigSvcReader(const std::string& name, ISvcLocator* pSvcLocator ) 
  : AthAlgorithm( name, pSvcLocator ), 
   m_outputFile("TrigConfigSvcReader.root"),
   m_trigconf( "TrigConf::TrigConfigSvc/TrigConfigSvc", name )
{

  declareProperty( "OutputFile", m_outputFile ); 
 
}

TrigConfigSvcReader::~TrigConfigSvcReader() {

}

StatusCode TrigConfigSvcReader::initialize() {

  if (m_trigconf.retrieve().isFailure()) {
    ATH_MSG_INFO("failed to retrieve the TrigConfSvc");
    return StatusCode::FAILURE;
  }
   
  ATH_MSG_INFO ( "masterKey=" << m_trigconf->masterKey() );
  ATH_MSG_INFO ( "lvl1PrescaleKey=" << m_trigconf->lvl1PrescaleKey() );
  ATH_MSG_INFO ( "hltPrescaleKey=" << m_trigconf->hltPrescaleKey() );

  std::filebuf fb;

  // ---------------------------------------------------------------------------------------------
  
  ATH_MSG_INFO("TrigConfigSvc::ctpCofig()->menu().items: item.print():");
  for (TrigConf::TriggerItem* item : m_trigconf->ctpConfig()->menu().items())
    item->print();
  
  const TrigConf::Menu* menu = &m_trigconf->ctpConfig()->menu();

  fb.open ("ctpConfig.menu.items.xml",std::ios::out);
  if (!fb.is_open()) 
    ATH_MSG_INFO("failed to open ctpConfig.menu.items.xml");

  std::ostream osMenuItems(&fb);
  menu->writeXMLItems(osMenuItems);
  fb.close();

  //ATH_MSG_INFO("menu.writeXMLItems");
  //menu->writeXMLItems(msg().stream());
  //ATH_MSG_INFO("menu.writeXMLItems done");

  fb.open ("ctpConfig.menu.thresholds.xml",std::ios::out);
  std::ostream osMenuThresholds(&fb);
  menu->writeXMLThresholds(osMenuThresholds);
  fb.close();

  fb.open ("ctpConfig.menu.MonCounters.xml",std::ios::out);
  std::ostream osMenuMonCounters(&fb);
  menu->writeXMLMonCounters(osMenuMonCounters);
  fb.close();

  fb.open ("ctpConfig.PrescaleSet.xml",std::ios::out);
  std::ostream osPrescaleSet(&fb);
  m_trigconf->ctpConfig()->prescaleSet().writeXML(osPrescaleSet);
  fb.close();

  fb.open ("ctpConfig.Random.xml",std::ios::out);
  std::ostream osRandom(&fb);
  m_trigconf->ctpConfig()->random().writeXML(osRandom);
  fb.close();

  fb.open ("ctpConfig.PrescaledClock.xml",std::ios::out);
  std::ostream osPrescaledClock(&fb);
  m_trigconf->ctpConfig()->prescaledClock().writeXML(osPrescaledClock);
  fb.close();

  // ---------------------------------------------------------------------------------------------
  ofstream fHLTchains("HLT.chains.xml",std::ofstream::out);
  for (TrigConf::HLTChain* chain : m_trigconf->chains() ) {
    ATH_MSG_INFO(" HLTChain=" <<  chain->chain_name() );
    chain->writeXML(fHLTchains);
  }  
  fHLTchains.close();

  ofstream fHLTsequences("HLT.sequences.xml",std::ofstream::out);
  for (TrigConf::HLTSequence* seq : m_trigconf->sequences() ) {
    ATH_MSG_INFO(" HLTSequence=" <<  seq->name() );
    seq->writeXML(fHLTsequences);
  }  
  fHLTsequences.close();

  // ---------------------------------------------------------------------------------------------

  //m_trigconf->bunchGroupSet()->bunchGroups();
  const TrigConf::BunchGroupSet* bunchGroupSet = m_trigconf->bunchGroupSet();
  
  for (uint16_t value : bunchGroupSet->bgPattern())
    ATH_MSG_INFO("BunchGroupSet::bgPattern: " << value);    

  //const TrigConf::BunchGroupSet bunchGroupSet = m_trigconf->ctpConfig()->bunchGroupSet();
  //vector<TrigConf::BunchGroup> bunchGroups = bunchGroupSet.bunchGroups();
  //bunchGroups.clear();
  //TrigConf::BunchGroup bg;
  //bg.setInternalNumber(0);
  //bg.setPartition(0);
  //bg.addBunch(1);
  //bunchGroupSet.fillPattern(0,bg);
  //ATH_MSG_INFO ("bunchGroupSet->menuPartition()=" << bunchGroupSet->menuPartition());

  ATH_MSG_INFO("bunchGroupSet->print():");
  bunchGroupSet->print();

  const std::vector<TrigConf::BunchGroup> bunchGroups = bunchGroupSet->bunchGroups();
  for (const TrigConf::BunchGroup bunchGroup : bunchGroups) {
    ATH_MSG_INFO ( "BunchGroup internalNumber=" << bunchGroup.internalNumber() << " partition=" << bunchGroup.partition() );
    for (int bcid : bunchGroup.bunches() )
      ATH_MSG_INFO ( "  bcid=" << bcid ); 
  }

  fb.open ("ctpConfig.BunchGroupSet.xml",std::ios::out);
  std::ostream osBunchGroupSet(&fb);
  bunchGroupSet->writeXML(osBunchGroupSet);
  fb.close();

  //const vector<uint32_t> bgPattern = bunchGroupSet->bgPattern();

  //fb.open ("bunchGroupSet.xml",std::ios::out);
  //std::ostream os(&fb);
  //bunchGroupSet->writeXML(os);
  //fb.close();

  // ------------------------------------------------------------------------------------------------

  fb.open ("thresholdConfig.CaloInfo.xml",std::ios::out);
  std::ostream osCaloInfo(&fb);
  const TrigConf::CaloInfo* caloInfo = &m_trigconf->thresholdConfig()->caloInfo();
  caloInfo->writeXML(osCaloInfo);
  fb.close();

  // ------------------------------------------------------------------------------------------------
  fb.open("muctpiConfig.xml",std::ios::out);
  std::ostream osMuCtpi(&fb);
  m_trigconf->muctpiConfig()->writeXML(osMuCtpi);
  fb.close();

  //
  //ATH_MSG_INFO ("configurationSource=" << m_trigconf->configurationSource() );
 
  return StatusCode::SUCCESS;
}

StatusCode TrigConfigSvcReader::execute() {

  //return StatusCode::SUCCESS;

  //m_tree->Fill();

  static bool first_time = true;

  if (first_time) {

    //const TrigConf::Menu* menu = &m_trigconf->ctpConfig()->menu();
    //ATH_MSG_INFO("menu=" << menu);

    //std::filebuf fb;
    //fb.open ("ctpConfig.menu.items.xml",std::ios::trunc);
    //std::ostream osMenuItems(&fb);
    //menu->writeXMLItems(osMenuItems);
    //fb.close();

    //ATH_MSG_INFO("menu.writeXMLItems");
    //menu->writeXMLItems(msg().stream());
    //ATH_MSG_INFO("menu.writeXMLItems done"); 

    //first_time = false;
  }

  return StatusCode::SUCCESS;
}

StatusCode TrigConfigSvcReader::finalize() {
  //m_file->cd();
  //m_tree->Write();
  //m_file->Close();
  return StatusCode::SUCCESS;
}



#ifndef READ_CONTAINER_PIXELCONDITIONSSUMMARYREADER_H
#define READ_CONTAINER_PIXELCONDITIONSSUMMARYREADER_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" //included under assumption you'll want to use some tools! Remove if you don't!

#include "InDetIdentifier/PixelID.h"
//#include "InDetConditionsSummaryService/IInDetConditionsSvc.h"
//#include "InDetConditionsSummaryService/IInDetConditionsSvc.h"
#include "PixelConditionsData/SpecialPixelMap.h"

#include "GaudiKernel/ServiceHandle.h"

#include <TFile.h>
#include <TTree.h>

#include <fstream>
using namespace std;
#include <stdlib.h>

class PixelConditionsSummaryReader: public ::AthAlgorithm { 
 public: 
  PixelConditionsSummaryReader( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~PixelConditionsSummaryReader(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

 private: 

  const PixelID* m_pixelID;

  //ServiceHandle<IInDetConditionsSvc> m_pixelSummarySvc;

  DetectorSpecialPixelMap* m_specialPixelMap;

}; 

#endif //> !READ_CONTAINER_PIXELCONDITIONSSUMMARYREADER_H

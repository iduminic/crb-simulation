#ifndef READ_CONTAINER_SICLUSTERREADER_H
#define READ_CONTAINER_SICLUSTERREADER_H

#include "AthenaBaseComps/AthAlgorithm.h"
//#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" //included under assumption you'll want to use some tools! Remove if you don't!

#include "InDetPrepRawData/PixelClusterContainer.h"
#include "InDetPrepRawData/SCT_ClusterContainer.h"

#include <TFile.h>
#include <TTree.h>
#include <TDirectory.h>

#include <vector>
using namespace std;

class SiClusterReader: public ::AthAlgorithm { 
//class SiClusterReader: public ::AthAnalysisAlgorithm { 
 public: 
  SiClusterReader( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~SiClusterReader(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

 private: 

  vector<float> m_pix_globalPosX; // PixelCluster.h
  vector<float> m_pix_globalPosY;
  vector<float> m_pix_globalPosZ; 
  vector<float> m_pix_omegax;
  vector<float> m_pix_omegay;
  vector<float> m_pix_totalCharge;
  vector<float> m_pix_energyLoss;
  vector<float> m_pix_isFake;
  vector<float> m_pix_isAmbiguous;
  vector<float> m_pix_isSplit;
  vector<float> m_pix_width_z;
  vector<float> m_pix_width_phiR; // SiCluster.h

  vector<int> m_pix_isPixel; // SiDetectorElement.h
  vector<int> m_pix_isSCT;
  vector<int> m_pix_isBarrel;    
  vector<int> m_pix_isEndcap;
  vector<int> m_pix_isBlayer; 
  vector<int> m_pix_isInnermostPixelLayer;
  vector<int> m_pix_isNextToInnermostPixelLayer;
  vector<int> m_pix_isDBM;

  vector<vector<int>> m_pix_totList;
  vector<int> m_pix_totalToT;

  vector<float> m_sct_globalPosX; // SiCluster.h
  vector<float> m_sct_globalPosY;
  vector<float> m_sct_globalPosZ; 
  vector<float> m_sct_width_z;
  vector<float> m_sct_width_phiR;
  vector<int> m_sct_isPixel; // SiDetectorElement.h
  vector<int> m_sct_isSCT;
  vector<int> m_sct_isBarrel;    
  vector<int> m_sct_isEndcap;
  vector<int> m_sct_isBlayer; 

  TFile* m_file;
  TTree* m_tree;
  TString m_treeFolder;

}; 

#endif //> !READ_CONTAINER_SICLUSTERREADER_H

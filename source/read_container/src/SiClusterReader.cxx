// read_container includes
#include "SiClusterReader.h"

SiClusterReader::SiClusterReader( const std::string& name, ISvcLocator* pSvcLocator ) 
: AthAlgorithm( name, pSvcLocator ){
//: AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty ); //example property declaration

}


SiClusterReader::~SiClusterReader() {}


StatusCode SiClusterReader::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //m_file = TFile::Open("SiClusters.root","recreate");
  m_tree = new TTree("CollectionTree","PixelClusters");
  m_treeFolder = gDirectory->GetPath();

  m_tree->Branch("PixelClusters.globalPositionX","vector<float>",&m_pix_globalPosX);
  m_tree->Branch("PixelClusters.globalPositionY","vector<float>",&m_pix_globalPosY);
  m_tree->Branch("PixelClusters.globalPositionZ","vector<float>",&m_pix_globalPosZ); 
  m_tree->Branch("PixelClusters.omegax","vector<float>",&m_pix_omegax);
  m_tree->Branch("PixelClusters.omegay","vector<float>",&m_pix_omegay);
  m_tree->Branch("PixelClusters.energLoss","vector<float>",&m_pix_energyLoss);
  m_tree->Branch("PixelClusters.totalCharge","vector<float>",&m_pix_totalCharge);
  m_tree->Branch("PixelClusters.isFake","vector<float>",&m_pix_isFake);
  m_tree->Branch("PixelClusters.isAmbiguous","vector<float>",&m_pix_isAmbiguous);
  m_tree->Branch("PixelClusters.isSplit","vector<float>",&m_pix_isSplit);
  m_tree->Branch("PixelClusters.width.z","vector<float>",&m_pix_width_z);
  m_tree->Branch("PixelClusters.width.phiR","vector<float>",&m_pix_width_phiR);
  m_tree->Branch("PixelClusters.isPixel","vector<float>",&m_pix_isPixel);
  m_tree->Branch("PixelClusters.isSCT","vector<float>",&m_pix_isSCT);
  m_tree->Branch("PixelClusters.isBarrel","vector<float>",&m_pix_isBarrel);
  m_tree->Branch("PixelClusters.isEndcap","vector<float>",&m_pix_isEndcap);
  m_tree->Branch("PixelClusters.isBlayer","vector<float>",&m_pix_isBlayer);
  m_tree->Branch("PixelClusters.isInnermostPixelLayer","vector<float>",&m_pix_isInnermostPixelLayer);
  m_tree->Branch("PixelClusters.isNextToInnermostPixelLayer","vector<float>",&m_pix_isNextToInnermostPixelLayer);
  m_tree->Branch("PixelClusters.isDBM","vector<float>",&m_pix_isDBM);
  m_tree->Branch("PixelClusters.totList","vector<vector<int>>",&m_pix_totList);
  m_tree->Branch("PixelClusters.totalToT","vector<int>",&m_pix_totalToT);

  m_tree->Branch("SCT_Clusters.globalPositionX","vector<float>",&m_sct_globalPosX);
  m_tree->Branch("SCT_Clusters.globalPositionY","vector<float>",&m_sct_globalPosY);
  m_tree->Branch("SCT_Clusters.globalPositionZ","vector<float>",&m_sct_globalPosZ); 
  m_tree->Branch("SCT_Clusters.width.z","vector<float>",&m_sct_width_z);
  m_tree->Branch("SCT_Clusters.width.phiR","vector<float>",&m_sct_width_phiR);
  m_tree->Branch("SCT_Clusters.isPixel","vector<float>",&m_sct_isPixel);
  m_tree->Branch("SCT_Clusters.isSCT","vector<float>",&m_sct_isSCT);
  m_tree->Branch("SCT_Clusters.isBarrel","vector<float>",&m_sct_isBarrel);
  m_tree->Branch("SCT_Clusters.isEndcap","vector<float>",&m_sct_isEndcap);
 
  return StatusCode::SUCCESS;
}

StatusCode SiClusterReader::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  ATH_MSG_INFO("tree entries= " << m_tree->GetEntries() );
  //gDirectory->cd(m_treeFolder);
  m_file = TFile::Open("SiClusters.root","recreate");
  m_tree->Write();
  m_file->Close();

  return StatusCode::SUCCESS;
}

StatusCode SiClusterReader::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  m_pix_globalPosX.clear();
  m_pix_globalPosY.clear();
  m_pix_globalPosZ.clear(); 
  m_pix_omegax.clear();
  m_pix_omegay.clear();
  m_pix_totalCharge.clear();
  m_pix_energyLoss.clear();  
  m_pix_isFake.clear();
  m_pix_isAmbiguous.clear();
  m_pix_isSplit.clear();
  m_pix_width_z.clear();
  m_pix_width_phiR.clear();

  m_pix_isPixel.clear();
  m_pix_isSCT.clear();
  m_pix_isBarrel.clear();
  m_pix_isEndcap.clear();
  m_pix_isBlayer.clear(); 
  m_pix_isInnermostPixelLayer.clear();
  m_pix_isNextToInnermostPixelLayer.clear();
  m_pix_isDBM.clear();

  m_pix_totList.clear();
  m_pix_totalToT.clear();
 
  const InDet::PixelClusterContainer* pixelClusterContainer = 0;
  if (evtStore()->retrieve(pixelClusterContainer, "PixelClusters").isFailure()) {
    ATH_MSG_INFO ("Failed to retrieve the PixelClusters");
    return StatusCode::FAILURE; 
  }

  int colNum = 0;
  for (InDet::PixelClusterContainer::const_iterator col = pixelClusterContainer->begin();
          col!= pixelClusterContainer->end(); col++) {
    ATH_MSG_INFO("PixelClusterContainer collection #" << colNum);
    colNum++;

    for (InDet::PixelClusterCollection::const_iterator clus = (*col)->begin();
          clus!=(*col)->end();clus++) {
      //ATH_MSG_INFO ("  PixelCluster omegax=" << (*clus)->omegax() 
      //                           << " omegay=" << (*clus)->omegay()
      //                           << " energyLoss=" << (*clus)->energyLoss() 
      //                           << " isFake=" << (*clus)->isFake()
      //                           << " isAmbiguous=" << (*clus)->isAmbiguous()
      //                           << " isSplit=" << (*clus)->isSplit()
      //               );
      m_pix_globalPosX.push_back((*clus)->globalPosition().x());
      m_pix_globalPosY.push_back((*clus)->globalPosition().y());
      m_pix_globalPosZ.push_back((*clus)->globalPosition().z()); 
      m_pix_omegax.push_back((*clus)->omegax());
      m_pix_omegay.push_back((*clus)->omegay()); 
      m_pix_energyLoss.push_back((*clus)->energyLoss()); 
      m_pix_totalCharge.push_back((*clus)->totalCharge()); 
      m_pix_isFake.push_back((*clus)->isFake());
      m_pix_isAmbiguous.push_back((*clus)->isAmbiguous());
      m_pix_isSplit.push_back((*clus)->isSplit());
      m_pix_width_z.push_back((*clus)->width().z());
      m_pix_width_phiR.push_back((*clus)->width().phiR());

      m_pix_isPixel.push_back((*clus)->detectorElement()->isPixel());
      m_pix_isSCT.push_back((*clus)->detectorElement()->isSCT());
      m_pix_isBarrel.push_back((*clus)->detectorElement()->isBarrel());
      m_pix_isEndcap.push_back((*clus)->detectorElement()->isEndcap());
      m_pix_isBlayer.push_back((*clus)->detectorElement()->isBlayer()); 
      m_pix_isInnermostPixelLayer.push_back((*clus)->detectorElement()->isInnermostPixelLayer());
      m_pix_isNextToInnermostPixelLayer.push_back((*clus)->detectorElement()->isNextToInnermostPixelLayer());
      m_pix_isDBM.push_back((*clus)->detectorElement()->isDBM());

      m_pix_totList.push_back((*clus)->totList());
      m_pix_totalToT.push_back((*clus)->totalToT());
   }
  }

  //  ------------------------------------------------------------------------------------
  m_sct_globalPosX.clear();
  m_sct_globalPosY.clear();
  m_sct_globalPosZ.clear(); 
  m_sct_width_z.clear();
  m_sct_width_phiR.clear();

  m_sct_isPixel.clear();
  m_sct_isSCT.clear();
  m_sct_isBarrel.clear();
  m_sct_isEndcap.clear();
 
  const InDet::SCT_ClusterContainer* sctClusterContainer = 0;
  if (evtStore()->retrieve(sctClusterContainer, "SCT_Clusters").isFailure()) {
    ATH_MSG_INFO ("Failed to retrieve the SCT_Clusters");
    return StatusCode::FAILURE; 
  }

  colNum = 0;
  for (InDet::SCT_ClusterContainer::const_iterator col = sctClusterContainer->begin();
          col!= sctClusterContainer->end(); col++) {
    ATH_MSG_INFO("SCT_ClusterContainer collection #" << colNum);
    colNum++;

    for (InDet::SCT_ClusterCollection::const_iterator clus = (*col)->begin();
          clus!=(*col)->end();clus++) {
     
      m_sct_globalPosX.push_back((*clus)->globalPosition().x());
      m_sct_globalPosY.push_back((*clus)->globalPosition().y());
      m_sct_globalPosZ.push_back((*clus)->globalPosition().z()); 
      m_sct_width_z.push_back((*clus)->width().z());
      m_sct_width_phiR.push_back((*clus)->width().phiR());

      m_sct_isPixel.push_back((*clus)->detectorElement()->isPixel());
      m_sct_isSCT.push_back((*clus)->detectorElement()->isSCT());
      m_sct_isBarrel.push_back((*clus)->detectorElement()->isBarrel());
      m_sct_isEndcap.push_back((*clus)->detectorElement()->isEndcap());
    }
  }

  m_tree->Fill();

  return StatusCode::SUCCESS;
}


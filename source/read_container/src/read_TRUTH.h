#ifndef READ_TRUTH_H
#define READ_TRUTH_H

#include <TFile.h>
#include <TTree.h>
#include <vector>
#include <string>
using namespace std;

//#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GeneratorObjects/McEventCollection.h"

#include "EventInfo/EventInfo.h"
#include "EventInfo/EventID.h"
#include "EventInfo/EventType.h"
#include "EventInfo/TriggerInfo.h"

//class read_TRUTH: public ::AthAnalysisAlgorithm { 
class read_TRUTH: public ::AthAlgorithm { 
 public: 
  read_TRUTH( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~read_TRUTH(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();
  
  virtual StatusCode beginInputFile();

 private: 

  string m_mcCollection;
  string m_outputFile;

  TTree* m_tree;

  unsigned int m_run_number;
  unsigned int m_event_number;

  int m_n;
  vector<int> m_pdg_id;
  vector<int> m_status;

  vector<float> m_momentum_px;
  vector<float> m_momentum_py;
  vector<float> m_momentum_pz;
  vector<float> m_momentum_e;

  vector<double> m_weights;

  vector<float> m_vertex_point3d_x;
  vector<float> m_vertex_point3d_y;
  vector<float> m_vertex_point3d_z;

  vector<float> m_vertex_position_x;
  vector<float> m_vertex_position_y;
  vector<float> m_vertex_position_z;
  vector<float> m_vertex_position_t;
  vector<float> m_vertex_position_theta;
  vector<float> m_vertex_position_phi;
  vector<float> m_vertex_position_eta;
  
}; 

#endif 

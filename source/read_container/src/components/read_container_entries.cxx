
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../read_HITSAlg.h"
DECLARE_ALGORITHM_FACTORY( read_HITSAlg )


#include "../read_TRUTH.h"
DECLARE_ALGORITHM_FACTORY( read_TRUTH )


#include "../SiSpacePointsReaderAlg.h"
DECLARE_ALGORITHM_FACTORY( SiSpacePointsReaderAlg )


#include "../SiClusterReader.h"
DECLARE_ALGORITHM_FACTORY( SiClusterReader )


#include "../RDOReader.h"
DECLARE_ALGORITHM_FACTORY( RDOReader )

#include "../PixelConditionsSummaryReader.h"
DECLARE_ALGORITHM_FACTORY( PixelConditionsSummaryReader )

#include "../ncb_analysis.h"
DECLARE_ALGORITHM_FACTORY( ncb_analysis )

#include "../TrigConfigSvcReader.h"
DECLARE_ALGORITHM_FACTORY( TrigConfigSvcReader )

#include "../TriggerDecisionReader.h"
DECLARE_ALGORITHM_FACTORY( TriggerDecisionReader )

DECLARE_FACTORY_ENTRIES( read_container ) 
{
  DECLARE_ALGORITHM( PixelConditionsSummaryReader );
  DECLARE_ALGORITHM( PixelRDOReader );
  DECLARE_ALGORITHM( SiClusterReader );
  DECLARE_ALGORITHM( SiSpacePointsReaderAlg );
  DECLARE_ALGORITHM( read_TRUTH );
  DECLARE_ALGORITHM( read_HITSAlg );
  DECLARE_ALGORITHM( read_EVNTAlg );
  DECLARE_ALGORITHM( ncb_analysis ); 
}

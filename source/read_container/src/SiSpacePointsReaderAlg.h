#ifndef SISPACEPOINTSREADER_SISPACEPOINTSREADERALG_H
#define SISPACEPOINTSREADER_SISPACEPOINTSREADERALG_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" //included under assumption you'll want to use some tools! Remove if you don't!

#include "TrkSpacePoint/SpacePointContainer.h"

#include <TTree.h>
#include <TFile.h>

#include <vector>
using namespace std;
class SiSpacePointsReaderAlg: public ::AthAlgorithm { 
 public: 
  SiSpacePointsReaderAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~SiSpacePointsReaderAlg(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

 private: 

  TTree* m_tree;

  int m_pixel_n;
  vector<float> m_pixel_eta;
  vector<float> m_pixel_phi;
  vector<float> m_pixel_r; 
  vector<float> m_pixel_x;
  vector<float> m_pixel_y;
  vector<float> m_pixel_z; 

  int m_sct_n;
  vector<float> m_sct_eta;
  vector<float> m_sct_phi;
  vector<float> m_sct_r;
  vector<float> m_sct_x;
  vector<float> m_sct_y;
  vector<float> m_sct_z;

}; 

#endif //> !SISPACEPOINTSREADER_SISPACEPOINTSREADERALG_H

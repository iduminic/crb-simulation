#ifndef READ_HITS_READ_HITSALG_H
#define READ_HITS_READ_HITSALG_H

#include <TFile.h>
#include <TString.h>

#include <TString.h>
#include <TH2D.h>
#include <TH1F.h>
#include <TBranch.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>

#include <vector>
#include <string>
#include <map>
using namespace std;
#include <stdlib.h>

//#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "AthenaBaseComps/AthAlgorithm.h"

#include "TileSimEvent/TileHitVector.h"
#include "LArSimEvent/LArHitContainer.h"
#include "InDetIdentifier/PixelID.h"
#include "InDetSimEvent/SiHitCollection.h"
//#include "InDetSimEventTPCnv/InDetHits/TRT_HitCollection_p3.h"
#include "GeneratorObjects/McEventCollection.h"
#include "CaloSimEvent/CaloCalibrationHitContainer.h"
#include "GeoAdaptors/GeoSiHit.h"
#include "GeoAdaptors/GeoLArHit.h"
#include "GeoAdaptors/GeoMuonHits.h"
#include "MuonSimEvent/CSCSimHitCollection.h"
#include "MuonSimEvent/MDTSimHitCollection.h"
#include "MuonSimEvent/RPCSimHitCollection.h"
#include "MuonSimEvent/TGCSimHitCollection.h"
#include "RecEvent/RecoTimingObj.h"
//#include "HitManagement/AthenaHitsVector.h"
//#include "G4SimTPCnv/TrackRecordCollection_p2.h"
#include "EventInfo/EventInfo.h"
#include "EventInfo/EventID.h"
#include "EventInfo/EventType.h"
#include "EventInfo/TriggerInfo.h"
#include "StoreGate/DataHandle.h"
//#include "InDetSimEvent/TRTUncompressedHitCollection.h"
#include "InDetSimEvent/SiHitIdHelper.h"
//#include "SimHelpers/AthenaHitsCollectionHelper.h"

class read_HITSAlg
//:public ::AthAnalysisAlgorithm
: public ::AthAlgorithm 
{ 
 public: 
  read_HITSAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~read_HITSAlg(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();
  
  virtual StatusCode beginInputFile();

 private: 

  unsigned int m_run_number;
  unsigned int m_event_number;

  vector<int> m_siHits_n;
  vector<vector<unsigned long int>> m_siHits_id;
  vector<vector<float>> m_siHits_meanTime;
  vector<vector<float>> m_siHits_energyLoss;
  vector<vector<float>> m_siHits_localStartPositionX;
  vector<vector<float>> m_siHits_localStartPositionY;
  vector<vector<float>> m_siHits_localStartPositionZ;
  vector<vector<short int>> m_siHits_barrelEndcap;
  vector<vector<short int>> m_siHits_layerDisk;
  vector<vector<short int>> m_siHits_etaModule;
  vector<vector<short int>> m_siHits_phiModule;
  vector<vector<short int>> m_siHits_side;
  vector<vector<short int>> m_siHits_eta_index;
  vector<vector<short int>> m_siHits_phi_index;
  vector<vector<short int>> m_siHits_is_barrel;
  vector<vector<short int>> m_siHits_layer_disk;
  vector<vector<short int>> m_siHits_is_blayer;
  vector<vector<short int>> m_siHits_isPixel;
  vector<vector<short int>> m_siHits_isSCT; 

  vector<vector<float>> m_siHits_globalPositionX;
  vector<vector<float>> m_siHits_globalPositionY; 
  vector<vector<float>> m_siHits_globalPositionZ;

  int m_trtHits_n; 

  vector<int> m_larHits_n;
  vector<vector<float>> m_larHits_energy;
  vector<vector<float>> m_larHits_time;
  vector<vector<float>> m_larHits_CaloDetDescrElement_x;
  vector<vector<float>> m_larHits_CaloDetDescrElement_y;
  vector<vector<float>> m_larHits_CaloDetDescrElement_z;
  vector<vector<int>> m_larHits_samplingLayer;

  vector<int> m_larCalibrationHits_n;

  int m_tileHits_n;
  int m_mbtsHits_n;

  int m_mdtHits_n;
  vector<float> m_mdtHits_globalTime;
  vector<float> m_mdtHits_driftRadius;
  vector<float> m_mdtHits_stepLength;
  vector<float> m_mdtHits_energyDeposit;
  vector<float> m_mdtHits_particleEncoding;
  vector<float> m_mdtHits_kineticEnergy;
  vector<float> m_mdtHits_meanTime;
  vector<float> m_mdtHits_trackNumber;
  vector<float> m_mdtHits_globalPositionX;
  vector<float> m_mdtHits_globalPositionY;
  vector<float> m_mdtHits_globalPositionZ;

  int m_cscHits_n;
  vector<float> m_cscHits_globalTime;
  vector<float> m_cscHits_energyDeposit;
  vector<float> m_cscHits_kineticEnergy;
  vector<float> m_cscHits_trackNumber;
  vector<float> m_cscHits_globalPositionX;
  vector<float> m_cscHits_globalPositionY;
  vector<float> m_cscHits_globalPositionZ;

  int m_tgcHits_n;

  int m_rpcHits_n;

  TTree* m_tree;

  string m_outputFile;
  bool m_isGeometryLoaded;
  bool m_doLArHits;

  //AthenaHitsCollectionHelper* m_hitCollectionHelper;

  const PixelID* m_PixelID;

}; 

#endif //> !READ_HITS_READ_HITSALG_H

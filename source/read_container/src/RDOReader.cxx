#include "RDOReader.h"

RDOReader::RDOReader( const std::string& name, ISvcLocator* pSvcLocator ) : 
  AthAlgorithm( name, pSvcLocator ),
  m_doBCM(true),
  m_doPixel(true)
{
  declareProperty( "doBCM", m_doBCM );
  declareProperty( "doPixel", m_doPixel );
}


RDOReader::~RDOReader() {}


StatusCode RDOReader::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  //m_file = TFile::Open("RDOReader.root","recreate");
  m_tree = new TTree("CollectionTree","");  

  m_tree->Branch("runNumber",&m_run_number,"run_number/i");
  m_tree->Branch("eventNumber",&m_event_number,"event_number/i");

  if (m_doBCM) {
    m_tree->Branch("BCM_RDO_Container_nCollections",&m_BCM_RDO_Container_nCollections);

    m_tree->Branch("BCM_RDO_Collection_nBCM_RDOs", "vector<unsigned int>",&m_BCM_RDO_Collection_nBCM_RDOs);
    m_tree->Branch("BCM_RDO_Collection_channel", "vector<unsigned int>",&m_BCM_RDO_Collection_channel);

    m_tree->Branch("BCM_RawData_word1", "vector<int>", &m_BCM_RawData_word1);
    m_tree->Branch("BCM_RawData_word2", "vector<int>", &m_BCM_RawData_word2);
    m_tree->Branch("BCM_RawData_channel", "vector<int>", &m_BCM_RawData_channel);
    m_tree->Branch("BCM_RawData_Pulse1Position","vector<int>",&m_BCM_RawData_pulse1Position);
    m_tree->Branch("BCM_RawData_Pulse1Width","vector<int>",&m_BCM_RawData_pulse1Width);
    m_tree->Branch("BCM_RawData_Pulse2Position","vector<int>",&m_BCM_RawData_pulse2Position);
    m_tree->Branch("BCM_RawData_Pulse2Width","vector<int>",&m_BCM_RawData_pulse2Width);
    m_tree->Branch("BCM_RawData_LVL1A","vector<int>",&m_BCM_RawData_LVL1A);
    m_tree->Branch("BCM_RawData_BCID","vector<int>",&m_BCM_RawData_BCID);
    m_tree->Branch("BCM_RawData_LVL1ID","vector<int>",&m_BCM_RawData_LVL1ID);
    m_tree->Branch("BCM_RawData_Error","vector<int>",&m_BCM_RawData_Error);
    
  }

  if (m_doPixel) {

    if (detStore()->retrieve(m_pixelID, "PixelID").isFailure()) {
      ATH_MSG_INFO ("failed to retrieve the PixelID");
      return StatusCode::FAILURE;
    } 

    m_tree->Branch("PixelRDORawData_id","vector<unsigned int long>",&m_pixelRDORawData_id);

    m_tree->Branch("PixelRDORawData_ToT","vector<int>",&m_pixelRDORawData_ToT);
    m_tree->Branch("PixelRDORawData_BCID","vector<int>",&m_pixelRDORawData_BCID);
    m_tree->Branch("PixelRDORawData_LVL1A","vector<int>",&m_pixelRDORawData_LVL1A);
    m_tree->Branch("PixelRDORawData_LVL1ID","vector<int>",&m_pixelRDORawData_LVL1ID); 

    m_tree->Branch("PixelRDORawData_is_barrel","vector<int>",&m_pixelRDORawData_is_barrel);
    m_tree->Branch("PixelRDORawData_is_dbm","vector<int>",&m_pixelRDORawData_is_dbm); 
    m_tree->Branch("PixelRDORawData_is_blayer","vector<int>",&m_pixelRDORawData_is_blayer);
    m_tree->Branch("PixelRDORawData_layer_disk","vector<int>",&m_pixelRDORawData_layer_disk);
    m_tree->Branch("PixelRDORawData_barrel_ec","vector<int>",&m_pixelRDORawData_barrel_ec);
    m_tree->Branch("PixelRDORawData_eta_module","vector<int>",&m_pixelRDORawData_eta_module);
    m_tree->Branch("PixelRDORawData_phi_module","vector<int>",&m_pixelRDORawData_phi_module); 
    m_tree->Branch("PixelRDORawData_eta_index","vector<int>",&m_pixelRDORawData_eta_index);
    m_tree->Branch("PixelRDORawData_phi_index","vector<int>",&m_pixelRDORawData_phi_index);

  }

  return StatusCode::SUCCESS;
}

StatusCode RDOReader::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  //gROOT->ProcessLine(".! pwd >> current_dir.txt");
  //string curr_dir;
  //ifstream ftxt("current_dir.txt");
  //ftxt >> curr_dir;
  //ftxt.close();
  //ATH_MSG_INFO("m_tree=" << m_tree << " current_dir=" << curr_dir);
 
  //if (TString(curr_dir.data()).Contains("worker_")) {
  //  ATH_MSG_INFO("Saving tree");
  //TFile* f = TFile::Open("RDOReader.root","recreate");
  ATH_MSG_INFO("tree entries=" << m_tree->GetEntries());
  //m_file->cd();
  m_file = TFile::Open("RDOReader.root","recreate"); 
  m_tree->Write();
  m_file->Close();
  //f->Close();
  //} 

  return StatusCode::SUCCESS;
}

StatusCode RDOReader::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  const EventInfo* eventInfo;
  if (evtStore()->retrieve(eventInfo, "McEventInfo").isFailure()) {
    ATH_MSG_INFO ("failed to retrieve the McEventInfo");
    return StatusCode::FAILURE;
  }

  EventID* eventID = eventInfo->event_ID();
  m_run_number = eventID->run_number();
  m_event_number = eventID->event_number();

  if (m_doBCM) {
    const BCM_RDO_Container* bcm_RDO_Container;
    if (evtStore()->retrieve(bcm_RDO_Container,"BCM_RDOs").isFailure()) {
      ATH_MSG_INFO ("failed to retrieve the BCM_RDO Container");
      return StatusCode::FAILURE;
    }
    m_BCM_RDO_Container_nCollections = bcm_RDO_Container->size();
    ATH_MSG_INFO ("BCM_RDO_Container retrieved. Number of collections=" << m_BCM_RDO_Container_nCollections );

    m_BCM_RDO_Collection_nBCM_RDOs.clear();
    m_BCM_RDO_Collection_channel.clear();

    m_BCM_RawData_word1.clear();
    m_BCM_RawData_word2.clear();
    m_BCM_RawData_channel.clear();
    m_BCM_RawData_pulse1Position.clear();
    m_BCM_RawData_pulse1Width.clear();
    m_BCM_RawData_pulse2Position.clear();
    m_BCM_RawData_pulse2Width.clear();
    m_BCM_RawData_LVL1A.clear();
    m_BCM_RawData_BCID.clear();
    m_BCM_RawData_LVL1ID.clear();
    m_BCM_RawData_Error.clear();

    for (unsigned int coll=0;coll<m_BCM_RDO_Container_nCollections;coll++) {
      ATH_MSG_INFO("  BCM_RDO_Collection #" << coll);
      const BCM_RDO_Collection* bcm_RDO_Collection = bcm_RDO_Container->at(coll);

      ATH_MSG_INFO("    #BCM_RawData=" << bcm_RDO_Collection->size() );
      m_BCM_RDO_Collection_nBCM_RDOs.push_back( bcm_RDO_Collection->size() );

      ATH_MSG_INFO("    getChannel-" << bcm_RDO_Collection->getChannel() );
      m_BCM_RDO_Collection_channel.push_back( bcm_RDO_Collection->getChannel() );

      for (unsigned int rdo = 0; rdo < bcm_RDO_Collection->size(); rdo++) {
        const BCM_RawData* bcm_RawData = bcm_RDO_Collection->at(rdo);
        ATH_MSG_INFO(" channel pulse1Position pulse2Position=" 
                     << bcm_RawData->getChannel() << " " << bcm_RawData->getPulse1Position() << " " << bcm_RawData->getPulse2Position() );
        m_BCM_RawData_word1.push_back(bcm_RawData->getWord1());
        m_BCM_RawData_word2.push_back(bcm_RawData->getWord2());
        m_BCM_RawData_channel.push_back(bcm_RawData->getChannel());
        m_BCM_RawData_pulse1Position.push_back(bcm_RawData->getPulse1Position());
        m_BCM_RawData_pulse1Width.push_back(bcm_RawData->getPulse1Width());
        m_BCM_RawData_pulse2Position.push_back(bcm_RawData->getPulse2Position());
        m_BCM_RawData_pulse2Width.push_back(bcm_RawData->getPulse2Width());
        m_BCM_RawData_LVL1A.push_back(bcm_RawData->getLVL1A());
        m_BCM_RawData_BCID.push_back(bcm_RawData->getBCID());
        m_BCM_RawData_LVL1ID.push_back(bcm_RawData->getLVL1ID());
        m_BCM_RawData_Error.push_back(bcm_RawData->getError());
      }

    }
  }

  if (m_doPixel) {

    m_pixelRDORawData_id.clear();
    m_pixelRDORawData_ToT.clear();
    m_pixelRDORawData_BCID.clear();
    m_pixelRDORawData_LVL1A.clear();
    m_pixelRDORawData_LVL1ID.clear();

    m_pixelRDORawData_is_barrel.clear();
    m_pixelRDORawData_is_dbm.clear();
    m_pixelRDORawData_is_blayer.clear();
    m_pixelRDORawData_layer_disk.clear();
    m_pixelRDORawData_barrel_ec.clear(); 
    m_pixelRDORawData_eta_module.clear();
    m_pixelRDORawData_phi_module.clear();
    m_pixelRDORawData_eta_index.clear();
    m_pixelRDORawData_phi_index.clear();

    // example of how to read the PixelRDO_Container is PixelClusterization.cxx
    const DataHandle<PixelRDO_Container> p_rdocontainer; 
    if (evtStore()->retrieve(p_rdocontainer,"PixelRDOs").isFailure()) {
      ATH_MSG_INFO ("failed to retrieve the PixelRDOs");
      return StatusCode::FAILURE;
    }   
    PixelRDO_Container::const_iterator rdoCollections = p_rdocontainer->begin();
    for (;rdoCollections!=p_rdocontainer->end();rdoCollections++) {
      ATH_MSG_INFO ("PixelRDO_Container: reading collection, size=" << (*rdoCollections)->size());
      InDetRawDataCollection<PixelRDORawData>::const_iterator pixelRDORawData=(*rdoCollections)->begin();
      for (; pixelRDORawData!=(*rdoCollections)->end();pixelRDORawData++) {

        //ATH_MSG_INFO (" PixelRDORawData getToT = " << (*pixelRDORawData)->getToT() 
        //                           << "getBCID = " << (*pixelRDORawData)->getBCID()
        //                           << "getLVL1A = " << (*pixelRDORawData)->getLVL1A() 
        //                           << "getLVL1ID = " << (*pixelRDORawData)->getLVL1ID() 
        //                           << "getWord = " << (*pixelRDORawData)->getWord() 
        //             );
        Identifier id = (*pixelRDORawData)->identify(); 
        //ATH_MSG_INFO ("             isBarrel=" << m_pixelID->is_barrel(id)
        //                     << " layer_disk=" << m_pixelID->layer_disk(id)
        //              );

        //ATH_MSG_INFO ( "  id.getString()=" << id.getString() );
        
        unsigned long id_long = strtol(id.getString().data(),0,16);
        //ATH_MSG_INFO ("  id long = " << id_long);

        m_pixelRDORawData_id.push_back(id_long);

        m_pixelRDORawData_ToT.push_back((*pixelRDORawData)->getToT());
        m_pixelRDORawData_BCID.push_back((*pixelRDORawData)->getBCID());
        m_pixelRDORawData_LVL1A.push_back((*pixelRDORawData)->getLVL1A());
        m_pixelRDORawData_LVL1ID.push_back((*pixelRDORawData)->getLVL1ID()); 

        m_pixelRDORawData_is_barrel.push_back(m_pixelID->is_barrel(id));
        m_pixelRDORawData_is_dbm.push_back(m_pixelID->is_dbm(id));
        m_pixelRDORawData_is_blayer.push_back(m_pixelID->is_blayer(id));
        m_pixelRDORawData_layer_disk.push_back(m_pixelID->layer_disk(id));
        m_pixelRDORawData_barrel_ec.push_back(m_pixelID->barrel_ec(id)); 
        m_pixelRDORawData_phi_module.push_back(m_pixelID->phi_module(id));
        m_pixelRDORawData_eta_module.push_back(m_pixelID->eta_module(id));
        m_pixelRDORawData_phi_index.push_back(m_pixelID->phi_index(id));
        m_pixelRDORawData_eta_index.push_back(m_pixelID->eta_index(id));
      }  
    }

  }
  // SCT_RDOs (SCT_RawDataContainer_p3) [InDet]
  //const DataHandle<SCT_RawDataContainer_p3> pSCT_RDOcontainer;

  //m_file->cd();
  m_tree->Fill();
  ATH_MSG_INFO("tree entries=" << m_tree->GetEntries());

  return StatusCode::SUCCESS;
}




 

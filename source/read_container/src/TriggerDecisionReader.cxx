#include "TriggerDecisionReader.h"

TriggerDecisionReader::TriggerDecisionReader(const std::string& name, ISvcLocator* pSvcLocator ) 
  : AthAlgorithm( name, pSvcLocator ), 
  m_outputFile("TriggerDecisionReader.root"),
  m_trigTool("Trig::TrigDecisionTool/TrigDecisionTool")
{

  declareProperty( "OutputFile", m_outputFile ); 
 
  m_trigItem_names = {
    "L1_RD0_UNPAIRED_ISO",
    "HLT_mb_sp_ncb_L1RD0_UNPAIRED_ISO",
    "HLT_noalg_bkg_L1RD0_UNPAIRED_ISO",

    "L1_BCM_AC_UNPAIRED_ISO",
    "L1_BCM_CA_UNPAIRED_ISO",
    "L1_BCM_Wide_UNPAIRED_ISO",
    "L1_BCM_AC_CA_UNPAIRED_ISO",

    "L1_BCM_AC_UNPAIRED_NONISO",
    "L1_BCM_CA_UNPAIRED_NONISO",
    "L1_BCM_Wide_UNPAIRED_NONISO",

    "L1_BCM_AC_CALIB",
    "L1_BCM_CA_CALIB",
    "L1_BCM_Wide_CALIB",

    "L1_BCM_AC_CA_BGRP0",

    "L1_BCM_AC_ABORTGAPNOTCALIB",
    "L1_BCM_CA_ABORTGAPNOTCALIB",
    "L1_BCM_Wide_ABORTGAPNOTCALIB",

    "L1_BCM_Wide_EMPTY",

    "L1_J12_UNPAIRED_ISO",
    "L1_J50_UNPAIRED_ISO",

    "L1_J12",
    "L1_J15",
    "L1_J20",
    "L1_J30",
    "L1_J50",
    "L1_J75",
    "L1_J100",
    "L1_J120",

    "L1_EM3",
    "L1_EM7",
    "L1_EM12",
    "L1_EM15",

    "HLT_noalg_bkg_L1Bkg" // this item is seeded by many background L1 items, such as L1_BCM*
  };

}

TriggerDecisionReader::~TriggerDecisionReader() {

}

StatusCode TriggerDecisionReader::initialize() {

  ATH_MSG_INFO("TriggerDecisionReader::initialize");
  gROOT->ProcessLine(".! pwd");

  //m_file=TFile::Open(m_outputFile.data(),"recreate");
  m_tree = new TTree("CollectionTree","");

  m_tree->Branch("runNumber",&m_runNumber,"runNumber/i");
  m_tree->Branch("eventNumber",&m_eventNumber,"eventNumber/i");
  m_tree->Branch("bcid",&m_bcid);

  m_trigItem_isPassed = vector<int>(m_trigItem_names.size(),0);

  for (unsigned int i=0;i<m_trigItem_names.size();i++) 
    m_tree->Branch(m_trigItem_names[i].data(), m_trigItem_isPassed.data() + i);
  
  return StatusCode::SUCCESS;
}

StatusCode TriggerDecisionReader::execute() {
  ATH_MSG_INFO("TriggerDecisionReader::execute:");

  gROOT->ProcessLine(".! pwd");

  const xAOD::EventInfo* eventInfo = 0;
  if (evtStore()->retrieve(eventInfo, "EventInfo").isFailure()) {
    ATH_MSG_INFO ("Failed to retrieve the EventInfo");
    //return StatusCode::FAILURE;
  }

  m_runNumber = eventInfo->runNumber();
  m_eventNumber = eventInfo->eventNumber();
  m_bcid = eventInfo->bcid();

  //static bool firstTime = true;
  //if (firstTime) {
  //  for (std::string trigger : m_trigTool->getListOfTriggers())
  //    ATH_MSG_INFO ( "getListOfTriggers(): " << trigger );
  //  firstTime = false;
  //}
  
  for (unsigned int i=0;i<m_trigItem_names.size();i++) {
    m_trigItem_isPassed[i] = m_trigTool->isPassed(m_trigItem_names[i]);
    cout << m_trigItem_names[i] << " isPassed=" << m_trigTool->isPassed(m_trigItem_names[i]) << endl;
  }

  m_tree->Fill();

  return StatusCode::SUCCESS;
}

StatusCode TriggerDecisionReader::finalize() {

  ATH_MSG_INFO("TriggerDecisionReader::finalize()");

  gROOT->ProcessLine(".! pwd");
  ATH_MSG_INFO( "saving tree with #entries=" << m_tree->GetEntries() );  

  m_file = TFile::Open(m_outputFile.data(),"recreate");
  //m_file->cd();
  m_tree->Write();
  m_file->Close();
  return StatusCode::SUCCESS;
}



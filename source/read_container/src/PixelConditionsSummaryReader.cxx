// read_container includes
#include "PixelConditionsSummaryReader.h"

PixelConditionsSummaryReader::PixelConditionsSummaryReader( const std::string& name, ISvcLocator* pSvcLocator ) 
  : AthAlgorithm( name, pSvcLocator )
    //, m_pixelSummarySvc("PixelConditionsSummarySvc", name)	
{

  //declareProperty( "Property", m_nProperty ); //example property declaration

}


PixelConditionsSummaryReader::~PixelConditionsSummaryReader() {}


StatusCode PixelConditionsSummaryReader::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  if (detStore()->retrieve(m_pixelID, "PixelID").isFailure()) {
    ATH_MSG_INFO ("failed to retrieve the PixelID");
    return StatusCode::FAILURE;
  }

  //PixelID::const_id_iterator id_iter = m_pixelID->wafer_begin();
  //for (; id_iter != m_pixelID->wafer_end(); id_iter++) {
  //  Identifier id = *id_iter;
  //} 

  //if (m_pixelSummarySvc.retrieve().isFailure()) {
  //  ATH_MSG_INFO ("failed to retrieve the PixelConditionsSummarySvc");
  //  return StatusCode::FAILURE;
 // }
 // else
 //   ATH_MSG_INFO ("PixelConditionsSummarySvc retrieved successfully"); 

  //ATH_MSG_INFO ("m_pixelSummarySvc->size()= (number of modules)=" << m_pixelSummarySvc.size() );

  return StatusCode::SUCCESS;
}

StatusCode PixelConditionsSummaryReader::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;
}

StatusCode PixelConditionsSummaryReader::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  if (detStore()->retrieve(m_specialPixelMap, "SpecialPixelMap").isFailure()) {
    ATH_MSG_INFO ("failed to retrieve the SpecialPixelMap");
    return StatusCode::FAILURE;
  }
  else ATH_MSG_INFO ("SpecialPixelMap retrieved successfully");

  ATH_MSG_INFO ( "m_specialPixelMap->size=" << m_specialPixelMap->size() );

  static bool first_time = 1;
  if (first_time) {
    first_time = 0;

    ATH_MSG_INFO ("Saving special pixels to file..");
    ofstream f_specialPixels("special_pixels.txt",ofstream::trunc);
    f_specialPixels << "module is_barrel is_blayer layer_disk phi_module eta_module phi_index eta_index (*pixel_it).first (*pixel_it).second" << endl;
    for (unsigned int module=0;module<m_specialPixelMap->size();module++) {

      //example code for how to get module Identifier from module IdentifierHash is PixelDigitizationTool.cxx
      Identifier moduleID = m_pixelID->wafer_id(IdentifierHash(module));

      PixelCoralClientUtils::ModuleSpecialPixelMap* moduleSpecialPixelMap = m_specialPixelMap->at(module);
      //ATH_MSG_INFO ("moduleSpecialPixelMap module# " << module << " size=" << moduleSpecialPixelMap->size() );
      PixelCoralClientUtils::ModuleSpecialPixelMap::const_iterator pixel_it = moduleSpecialPixelMap->begin(); 
   
      for (;pixel_it!=moduleSpecialPixelMap->end();pixel_it++) {
        f_specialPixels << module
                        << " " << m_pixelID->is_barrel(moduleID)
                        << " " << m_pixelID->is_blayer(moduleID)
                        << " " << m_pixelID->layer_disk(moduleID)
                        << " " << m_pixelID->phi_module(moduleID) 
                        << " " << m_pixelID->eta_module(moduleID) 
                        << " " << (*pixel_it).first << " " << (*pixel_it).second 
                        << endl;
      }     
    } 
    f_specialPixels.close();

    TFile* f1 = TFile::Open("PixelID.root","recreate"); 
    TTree* tree = new TTree("CollectionTree","PixelID data");

    int wafer_idHash;
    int wafer_chipsPerModule;
    int wafer_type;   

    unsigned long int id_long; 
    int is_barrel;
    int barrel_ec;
    int is_blayer;
    int layer_disk;
    int phi_module;
    int eta_module;
    int phi_index;
    int eta_index;

    tree->Branch("PixelID.id_long",&id_long,"id_long/l");
    tree->Branch("PixelID.is_barrel",&is_barrel,"is_barrel/I");
    tree->Branch("PixelID.barrel_ec",&barrel_ec,"barrel_ec/I");
    tree->Branch("PixelID.is_blayer",&is_blayer,"is_blayer/I");
    tree->Branch("PixelID.layer_disk",&layer_disk,"layer_disk/I"); 
    tree->Branch("PixelID.phi_module",&phi_module,"phi_module/I");
    tree->Branch("PixelID.eta_module",&eta_module,"eta_module/I"); 
    tree->Branch("PixelID.phi_index",&phi_index,"phi_index/I");
    tree->Branch("PixelID.eta_index",&eta_index,"eta_index/I");

    tree->Branch("PixelID.wafer_idHash",&wafer_idHash,"wafer_idHash/I");
    tree->Branch("PixelID.wafer_chipsPerModule",&wafer_chipsPerModule,"wafer_chipsPerModule/I");
    tree->Branch("PixelID.wafer_type",&wafer_type,"wafer_type/I");

    ofstream f2("PixelID.txt",ofstream::trunc);   
 
    //ATH_MSG_INFO ("  (number of Pixel modules?) m_pixelID->wafer_hash_max()=" << m_pixelID->wafer_hash_max() );
    // example code of how to loop on the pixel modules using the pixelID,
    // and how to check the status of the modules using the pixelSpecialMap tool is: PixelMapTestAlg.cxx
    //for (unsigned int i=0;i<m_pixelID->wafer_hash_max();i++) {          // loop on the pixel modules
    //  Identifier moduleID = m_pixelID->wafer_id(IdentifierHash(i));
    //  ATH_MSG_INFO ("module " << i << "specialPixelMap module status=" << m_specialPixelMap->module(i)->moduleStatus());
    //}
   
    ATH_MSG_INFO ("Iterating on all pixels using the PixelID and saving info to file .."); 
    PixelID::const_expanded_id_iterator pixel_it = m_pixelID->pixel_begin();
    for (;pixel_it!=m_pixelID->pixel_end();++pixel_it) { // ++pixel_it compiles, but pixel_id++ doesn't!   ++pixel_it is used is PixelID.cxx
      ExpandedIdentifier expPixelID = *pixel_it;
      Identifier id = m_pixelID->pixel_id(expPixelID); 

      Identifier wafer_id = m_pixelID->wafer_id(m_pixelID->barrel_ec(id), m_pixelID->layer_disk(id), m_pixelID->phi_module(id),m_pixelID->eta_module(id));
      IdentifierHash wafer_idHash = m_pixelID->wafer_hash(wafer_id);

      //PixelConditionsSummarySvc::getID:
      //unsigned int chipsPerModule = m_specialPixelMap->module(wafer_idHash)->chipsPerModule();
      //unsigned int chipType = m_specialPixelMap->module(wafer_idHash)->chipType();
      //chipType +=chipsPerModule*10; // PixelConditionsSummarySvc.cxx 
      //unsigned int pixID = m_specialPixelMap->encodePixelID(m_pixel_ID->barrel_ec(id), m_pixelID->phi_module(id),m_pixelID->eta_index(id),m_pixelID->phi_index(id), chipType);
      //ATH_MSG_INFO ("pixID=" << pixID);
     
      for (unsigned int i=0;i<expPixelID.fields();i++)
	f2 << expPixelID[i] << " ";

      f2 << id.getString();

      f2 << " wafer_idHash=" << wafer_idHash.value() << " "
                << " chipsPerModule=" << m_specialPixelMap->module(wafer_idHash)->chipsPerModule() << " "
                << " chipType=" << m_specialPixelMap->module(wafer_idHash)->chipType() << " ";

      f2 << m_pixelID->is_barrel(id) << " "
         << m_pixelID->barrel_ec(id) << " "
         << m_pixelID->is_blayer(id) << " "
         << m_pixelID->layer_disk(id) << " "
         << m_pixelID->phi_module(id) << " "
         << m_pixelID->eta_module(id) << " "
         << m_pixelID->phi_index(id) << " "
         << m_pixelID->eta_index(id) << " "
         << endl;

      wafer_idHash = wafer_idHash.value();
      wafer_chipsPerModule = m_specialPixelMap->module(wafer_idHash)->chipsPerModule();     
      wafer_type = m_specialPixelMap->module(wafer_idHash)->chipType(); 

      id_long = strtol(id.getString().data(),0,16);
      is_barrel = m_pixelID->is_barrel(id); 
      barrel_ec = m_pixelID->barrel_ec(id);   
      is_blayer = m_pixelID->is_blayer(id);
      layer_disk = m_pixelID->layer_disk(id);
      phi_module = m_pixelID->phi_module(id);
      eta_module = m_pixelID->eta_module(id); 
      phi_index = m_pixelID->phi_index(id);
      eta_index = m_pixelID->eta_index(id); 

      tree->Fill();

    }

    f2.close();
    f1->Close();

  }

  return StatusCode::SUCCESS;
}



// SiSpacePointsReader includes
#include "SiSpacePointsReaderAlg.h"



SiSpacePointsReaderAlg::SiSpacePointsReaderAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty ); //example property declaration

}


SiSpacePointsReaderAlg::~SiSpacePointsReaderAlg() {}


StatusCode SiSpacePointsReaderAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  m_tree = new TTree("CollectionTree","SiSpacePoints");

  m_tree->Branch("PixelSpacePoints_n",&m_pixel_n,"m_pixel_n/I");
  m_tree->Branch("PixelSpacePoints_r","vector<float>",&m_pixel_r);
  m_tree->Branch("PixelSpacePoints_eta","vector<float>",&m_pixel_eta);
  m_tree->Branch("PixelSpacePoints_phi","vector<float>",&m_pixel_phi);
  m_tree->Branch("PixelSpacePoints_x","vector<float>",&m_pixel_x);
  m_tree->Branch("PixelSpacePoints_y","vector<float>",&m_pixel_y);
  m_tree->Branch("PixelSpacePoints_z","vector<float>",&m_pixel_z);

  m_tree->Branch("SCTSpacePoints_n",&m_sct_n,"m_sct_n/I");
  m_tree->Branch("SCTSpacePoints_r","vector<float>",&m_sct_r);
  m_tree->Branch("SCTSpacePoints_eta","vector<float>",&m_sct_eta);
  m_tree->Branch("SCTSpacePoints_phi","vector<float>",&m_sct_phi);
  m_tree->Branch("SCTSpacePoints_x","vector<float>",&m_sct_x);
  m_tree->Branch("SCTSpacePoints_y","vector<float>",&m_sct_y);
  m_tree->Branch("SCTSpacePoints_z","vector<float>",&m_sct_z);

  return StatusCode::SUCCESS;
}

StatusCode SiSpacePointsReaderAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  TFile* f = TFile::Open("SiSpacePoints.root","recreate");
  ATH_MSG_INFO("saving tree with #entries=" << m_tree->GetEntries() );
  m_tree->Write();
  f->Close();

  return StatusCode::SUCCESS;
}

StatusCode SiSpacePointsReaderAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  m_pixel_n = 0;
  m_pixel_eta.clear();
  m_pixel_phi.clear();
  m_pixel_r.clear();
  m_pixel_x.clear();
  m_pixel_y.clear();
  m_pixel_z.clear(); 

  m_sct_n = 0;
  m_sct_eta.clear();
  m_sct_phi.clear();
  m_sct_r.clear();
  m_sct_x.clear();
  m_sct_y.clear();
  m_sct_z.clear();

  const SpacePointContainer* pixelSpacePointCont = 0;
  if (evtStore()->retrieve(pixelSpacePointCont, "PixelSpacePoints").isFailure()) {
    ATH_MSG_INFO( "failed to retrieve the PixelSpacePoints" ) ;
    return StatusCode::FAILURE;
  }
  
  ATH_MSG_INFO( "PixelSpacePoints retrieved successfully. numberOfCollections=" << pixelSpacePointCont->numberOfCollections() );

  // example for how to read the SpacePointContainer is: SiSpacePointsSeedMaker_ATLxk.cxx
  for (SpacePointContainer::const_iterator spc = pixelSpacePointCont->begin(); 
        spc!=pixelSpacePointCont->end(); spc++) {

    ATH_MSG_INFO ("PixelSpacePointContainer size=" <<  (*spc)->size());

    for (SpacePointCollection::const_iterator sp = (*spc)->begin(); sp!=(*spc)->end(); sp++) {
      ATH_MSG_INFO ("PixelSpacePoint r=" << (*sp)->r() << " eta=" << (*sp)->eta() << " phi=" << (*sp)->phi() );
      ATH_MSG_INFO ("  x,y,z=" << (*sp)->globalPosition().x() << " " << (*sp)->globalPosition().y() << " " << (*sp)->globalPosition().z() );
      m_pixel_n++;
      m_pixel_r.push_back((*sp)->r());
      m_pixel_eta.push_back((*sp)->eta()); 
      m_pixel_phi.push_back((*sp)->phi()); 
      m_pixel_x.push_back((*sp)->globalPosition().x());
      m_pixel_y.push_back((*sp)->globalPosition().y());
      m_pixel_z.push_back((*sp)->globalPosition().z());

    }
  }
      
  const SpacePointContainer* sctSpacePointCont = 0;
  if (evtStore()->retrieve(sctSpacePointCont, "SCT_SpacePoints").isFailure()) {
    ATH_MSG_INFO( "failed to retrieve the SCT_SpacePoints" ) ;
    return StatusCode::FAILURE;
  }
  
  ATH_MSG_INFO( "SCT_SpacePoints retrieved successfully. numberOfCollections=" << sctSpacePointCont->numberOfCollections() );

  for (SpacePointContainer::const_iterator spc = sctSpacePointCont->begin();
        spc!=sctSpacePointCont->end(); spc++) {

    ATH_MSG_INFO ("SCT SpacePointContainer size=" <<  (*spc)->size());

    for (SpacePointCollection::const_iterator sp = (*spc)->begin(); sp!=(*spc)->end(); sp++) {
      ATH_MSG_INFO ("SCT_SpacePoint r=" << (*sp)->r() << " eta=" << (*sp)->eta() << " phi=" << (*sp)->phi() );
      ATH_MSG_INFO ("  x,y,z=" << (*sp)->globalPosition().x() << " " << (*sp)->globalPosition().y() << " " << (*sp)->globalPosition().z() );
      m_sct_n++;
      m_sct_r.push_back((*sp)->r());
      m_sct_eta.push_back((*sp)->eta());
      m_sct_phi.push_back((*sp)->phi());
      m_sct_x.push_back((*sp)->globalPosition().x());
      m_sct_y.push_back((*sp)->globalPosition().y());
      m_sct_z.push_back((*sp)->globalPosition().z());

    }
  }
 
  m_tree->Fill(); 
 
  return StatusCode::SUCCESS;
}



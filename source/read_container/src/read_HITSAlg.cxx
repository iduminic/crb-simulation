// read_HITS includes
#include "read_HITSAlg.h"

const int N_HIT_CONTAINERS = 15;

// for the TRT the collection is called: TRTUncompressedHits (TRT_HitCollection_p3)
// is it not possible to read this one ?

const TString HIT_CONTAINERS[N_HIT_CONTAINERS] = {
  "BCMHits", "BLMHits",
  "PixelHits", "SCT_Hits",
  "LArHitEMB", "LArHitEMEC", "LArHitFCAL", "LArHitHEC",
  "MBTSHits", "TileHitVec",
  "CSC_Hits","MDT_Hits","TGC_Hits","RPC_Hits"
};

read_HITSAlg::read_HITSAlg( const std::string& name, ISvcLocator* pSvcLocator ) 
//    : AthAnalysisAlgorithm(name, pSvcLocator ),
  : AthAlgorithm( name, pSvcLocator ), 
    m_outputFile("read_HITS.root"),
    m_isGeometryLoaded(true),
    m_doLArHits(true)
{
  declareProperty( "outputFile", m_outputFile ); //example property declaration
  declareProperty( "isGeometryLoaded", m_isGeometryLoaded );
  declareProperty( "doLArHits", m_doLArHits );
  m_PixelID = 0;
}


read_HITSAlg::~read_HITSAlg() {}


StatusCode read_HITSAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
 
  m_tree = new TTree("CollectionTree","hits");

  m_tree->Branch("run_number",&m_run_number,"run_number/i");
  m_tree->Branch("event_number",&m_event_number,"event_number/i");

  m_tree->Branch("SiHits.n","vector<int>",&m_siHits_n);
  m_tree->Branch("SiHits.id","vector<vector<unsigned long int>>",&m_siHits_id);
  m_tree->Branch("SiHits.meanTime","vector<vector<float>>", &m_siHits_meanTime);
  m_tree->Branch("SiHits.energyLoss","vector<vector<float>>", &m_siHits_energyLoss);
  m_tree->Branch("SiHits.localStartPositionX","vector<vector<float>>", &m_siHits_localStartPositionX);
  m_tree->Branch("SiHits.localStartPositionY","vector<vector<float>>", &m_siHits_localStartPositionY);
  m_tree->Branch("SiHits.localStartPositionZ","vector<vector<float>>", &m_siHits_localStartPositionZ);
  m_tree->Branch("SiHits.globalPositionX","vector<vector<float>>", &m_siHits_globalPositionX);
  m_tree->Branch("SiHits.globalPositionY","vector<vector<float>>", &m_siHits_globalPositionY);
  m_tree->Branch("SiHits.globalPositionZ","vector<vector<float>>", &m_siHits_globalPositionZ);
  m_tree->Branch("SiHits.barrelEndcap","vector<vector<short int>>", &m_siHits_barrelEndcap);
  m_tree->Branch("SiHits.layerDisk","vector<vector<short int>>", &m_siHits_layerDisk);
  m_tree->Branch("SiHits.etaModule","vector<vector<short int>>", &m_siHits_etaModule);
  m_tree->Branch("SiHits.phiModule","vector<vector<short int>>", &m_siHits_phiModule);
  m_tree->Branch("SiHits.side","vector<vector<short int>>", &m_siHits_side);
  m_tree->Branch("SiHits.layer_disk","vector<vector<short int>>",&m_siHits_layer_disk);
  m_tree->Branch("SiHits.isPixel","vector<vector<short int>>",&m_siHits_isPixel); 
  m_tree->Branch("SiHits.isSCT","vector<vector<short int>>",&m_siHits_isSCT); 

  m_tree->Branch("SiHits.PixelID.eta_index","vector<vector<short int>>",&m_siHits_eta_index); // description of eta_index and phi_index in PixelID.h
  m_tree->Branch("SiHits.PixelID.phi_index","vector<vector<short int>>",&m_siHits_phi_index);
  m_tree->Branch("SiHits.PixelID.is_blayer","vector<vector<short int>>",&m_siHits_is_blayer);
  m_tree->Branch("SiHits.PixelID.is_barrel","vector<vector<short int>>",&m_siHits_is_barrel);

  m_tree->Branch("TRTHits.n", &m_trtHits_n, "m_trtHits_n/I");

  m_tree->Branch("LArHits.n","vector<int>",&m_larHits_n);
  m_tree->Branch("LArHits.energy","vector<vector<float>>",&m_larHits_energy);
  m_tree->Branch("LArHits.time","vector<vector<float>>",&m_larHits_time);
  m_tree->Branch("LArHits.CaloDetDescrElement.x","vector<vector<float>>",&m_larHits_CaloDetDescrElement_x);
  m_tree->Branch("LArHits.CaloDetDescrElement.y","vector<vector<float>>",&m_larHits_CaloDetDescrElement_y);
  m_tree->Branch("LArHits.CaloDetDescrElement.z","vector<vector<float>>",&m_larHits_CaloDetDescrElement_z);
  m_tree->Branch("LArHits.SamplingLayer","vector<vector<int>>",&m_larHits_samplingLayer);

  m_tree->Branch("LArCalibrationHits.n","vector<int>", &m_larCalibrationHits_n);

  m_tree->Branch("TileHits.n",&m_tileHits_n,"m_tileHits_n/I");
  m_tree->Branch("MBTSHits.n",&m_mbtsHits_n,"m_mbtsHits_n/I");

  m_tree->Branch("MDTHits.n",&m_mdtHits_n,"m_mdtHits_n/I");
  m_tree->Branch("MDTHits.globalTime","vector<float>",&m_mdtHits_globalTime);
  m_tree->Branch("MDTHits.driftRadius","vector<float>",&m_mdtHits_driftRadius);
  m_tree->Branch("MDTHits.stepLength","vector<float>",&m_mdtHits_stepLength);
  m_tree->Branch("MDTHits.energyDeposit","vector<float>",&m_mdtHits_energyDeposit);
  m_tree->Branch("MDTHits.particleEncoding","vector<float>",&m_mdtHits_particleEncoding);
  m_tree->Branch("MDTHits.kineticEnergy","vector<float>",&m_mdtHits_kineticEnergy);
  m_tree->Branch("MDTHits.meanTime","vector<float>",&m_mdtHits_meanTime);
  m_tree->Branch("MDTHits.trackNumber","vector<float>",&m_mdtHits_trackNumber);
  m_tree->Branch("MDTHits.globalPositionX","vector<float>",&m_mdtHits_globalPositionX);
  m_tree->Branch("MDTHits.globalPositionY","vector<float>",&m_mdtHits_globalPositionY);
  m_tree->Branch("MDTHits.globalPositionZ","vector<float>",&m_mdtHits_globalPositionZ);

  m_tree->Branch("CSCHits.n",&m_cscHits_n,"m_cscHits_n/I");
  m_tree->Branch("CSCHits.globalTime","vector<float>",&m_cscHits_globalTime);
  m_tree->Branch("CSCHits.energyDeposit","vector<float>",&m_cscHits_energyDeposit);
  m_tree->Branch("CSCHits.kineticEnergy","vector<float>",&m_cscHits_kineticEnergy);
  m_tree->Branch("CSCHits.trackNumber","vector<float>",&m_cscHits_trackNumber);
  m_tree->Branch("CSCHits.globalPositionX","vector<float>",&m_cscHits_globalPositionX);
  m_tree->Branch("CSCHits.globalPositionY","vector<float>",&m_cscHits_globalPositionY);
  m_tree->Branch("CSCHits.globalPositionZ","vector<float>",&m_cscHits_globalPositionZ);

  m_tree->Branch("TGCHits.n",&m_tgcHits_n,"m_tgcHits_n/I");

  m_tree->Branch("RPCHits.n",&m_rpcHits_n,"m_rpcHits_n/I");

  //m_hitCollectionHelper = new AthenaHitsCollectionHelper();

  if (m_isGeometryLoaded)
    if (detStore()->retrieve(m_PixelID, "PixelID").isFailure()) {
      ATH_MSG_INFO( "failed to retrieve the PixelID");
      return StatusCode::FAILURE;
    } 

  return StatusCode::SUCCESS;
}

StatusCode read_HITSAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  ATH_MSG_INFO("  m_tree->GetEntries() = " << m_tree->GetEntries() );

  gROOT->ProcessLine(".! pwd");

  TFile* fHist=TFile::Open(m_outputFile.data(),"recreate");
 
  m_tree->Write();

  fHist->Close();
 
  return StatusCode::SUCCESS;
}

StatusCode read_HITSAlg::execute() {  

  gROOT->ProcessLine(".! pwd");
 
  const EventInfo* eventInfo = 0;
  if (evtStore()->retrieve(eventInfo, "McEventInfo").isFailure()) {
    ATH_MSG_INFO ("Failed to retrieve the MCEventInfo");
    return StatusCode::FAILURE;
  }
 
  /*ATH_MSG_INFO ( "  event_ID run_number=" << eventInfo->event_ID()->run_number() );
  ATH_MSG_INFO ( "           event_number=" << eventInfo->event_ID()->event_number() );
  ATH_MSG_INFO ( "           lumi_block=" << eventInfo->event_ID()->lumi_block() );
  ATH_MSG_INFO ( "           bcid=" << eventInfo->event_ID()->bunch_crossing_id() );
  ATH_MSG_INFO ( "           detector_mask=" << eventInfo->event_ID()->detector_mask() );  

  ATH_MSG_INFO ( " event_type typeToString=" << eventInfo->event_type()->typeToString() );
  ATH_MSG_INFO ( "            get_detdescr_tags=" << eventInfo->event_type()->get_detdescr_tags() );
  ATH_MSG_INFO ( "            mc_channel_number=" << eventInfo->event_type()->mc_channel_number() );
  ATH_MSG_INFO ( "            mc_event_number=" << eventInfo->event_type()->mc_event_number() );
  ATH_MSG_INFO ( "            mc_event_weight=" << eventInfo->event_type()->mc_event_weight() );

  ATH_MSG_INFO ( "trigger_info statusElement=" << eventInfo->trigger_info()->statusElement() );
  ATH_MSG_INFO ( "             extendedLevel1ID=" << eventInfo->trigger_info()->extendedLevel1ID() );
  ATH_MSG_INFO ( "             level1TriggerType=" << eventInfo->trigger_info()->level1TriggerType() );

  ATH_MSG_INFO ( "  actualInteractionsPerCrossing=" << eventInfo->actualInteractionsPerCrossing());  */
 
  m_run_number = eventInfo->event_ID()->run_number();
  m_event_number = eventInfo->event_ID()->event_number();
  
  m_siHits_n.clear();
  m_siHits_id.clear();
  m_siHits_energyLoss.clear();
  m_siHits_meanTime.clear();
  m_siHits_localStartPositionX.clear();
  m_siHits_localStartPositionY.clear();
  m_siHits_localStartPositionZ.clear();
  m_siHits_globalPositionX.clear();
  m_siHits_globalPositionY.clear();
  m_siHits_globalPositionZ.clear();
  m_siHits_barrelEndcap.clear();
  m_siHits_layerDisk.clear();
  m_siHits_etaModule.clear();
  m_siHits_phiModule.clear();
  m_siHits_side.clear();
  m_siHits_eta_index.clear();
  m_siHits_phi_index.clear();
  m_siHits_is_barrel.clear();
  m_siHits_layer_disk.clear();
  m_siHits_is_blayer.clear();
  m_siHits_isPixel.clear(); 
  m_siHits_isSCT.clear();   

  const TString SiHitContainers[]={"BCMHits","BLMHits", "PixelHits","SCT_Hits"};
  const SiHitCollection* siHits[4];
  for (int i=0;i<4;i++) {

    if (evtStore()->retrieve(siHits[i],SiHitContainers[i].Data()).isFailure()) {
      ATH_MSG_INFO("Failed to read " << SiHitContainers[i] );
      return StatusCode::SUCCESS;
    }

    //ATH_MSG_INFO("#" << SiHitContainers[i] << ": " <<siHits[i]->Size());
  
    vector<unsigned long int> siHits_id;
    vector<float> siHits_energyLoss;
    vector<float> siHits_meanTime;
    vector<float> siHits_localStartPositionX;
    vector<float> siHits_localStartPositionY;
    vector<float> siHits_localStartPositionZ;
    vector<float> siHits_globalPositionX;
    vector<float> siHits_globalPositionY;
    vector<float> siHits_globalPositionZ;
    vector<short int> siHits_barrelEndcap;
    vector<short int> siHits_layerDisk;
    vector<short int> siHits_etaModule;
    vector<short int> siHits_phiModule;
    vector<short int> siHits_side;
    vector<short int> siHits_eta_index;
    vector<short int> siHits_phi_index;
    vector<short int> siHits_is_barrel;
    vector<short int> siHits_layer_disk;
    vector<short int> siHits_is_blayer;
    vector<short int> siHits_isPixel;
    vector<short int> siHits_isSCT;
    
    for (const SiHit siHit : *(siHits[i])) { //AtlasHitsVector<SiHit>
      
      // these are localPositions, not very useful
      //cout << "ABCD SiHit localStartPositionX = " << siHit.localStartPosition().x() << " endPositionX=" << siHit.localEndPosition().x() << endl;
      //cout << "ABCD SiHit localStartPositionY = " << siHit.localStartPosition().y() << " endPositionY=" << siHit.localEndPosition().y() << endl;
      //cout << "ABCD SiHit localStartPositionZ = " << siHit.localStartPosition().z() << " endPositionZ=" << siHit.localEndPosition().z() << endl;    

      Identifier id = Identifier(siHit.identify());

      //cout << "Id.show: ";
      //id.show();
      //cout << endl;

      unsigned long int id_long = strtol(id.getString().data(),0,16);
        
      //cout << SiHitContainers[i] << endl
      //     << "  identify=" << siHit.identify() << endl
      //     << "  id long=" << id_long << endl
      //     << "  energyLoss=" << siHit.energyLoss() << endl
      //     << "  meanTime=" << siHit.meanTime() << endl
      //     << "  barrelEndcap= " << siHit.getBarrelEndcap() << endl
      //     << "  layerDisk=" << siHit.getLayerDisk() << endl
      //     << "  etaModule=" << siHit.getEtaModule() << endl
      //     << "  phiModule=" << siHit.getPhiModule() << endl
      //     << "  isPixel=" << siHit.isPixel() << endl
      //     << "  isSCT=" << siHit.isSCT() << endl;
      //<< "  particleLink pdg_id=" << (*siHit.particleLink()).pdg_id() << endl

      //if (SiHitContainers[i] == "PixelHits") 
       // cout << "  Using the PixelID:" << endl
       //      << "    eta_module=" << m_PixelID->eta_module(id) << endl
       //      << "    phi_module=" << m_PixelID->phi_module(id) << endl
       //      << "    eta_index=" << m_PixelID->eta_index(id) << endl
       //      << "    phi_index=" << m_PixelID->phi_index(id) << endl
       //      << "    is_barrel=" << m_PixelID->is_barrel(id) << endl
       //      << "    layer_disk=" << m_PixelID->layer_disk(id) << endl
       //      << "    is_blayer=" << m_PixelID->is_blayer(id) << endl;

      //cout << "  Using the SiHitIdHelper" << endl
      //     << "    isPixel=" << SiHitIdHelper::GetHelper()->isPixel(siHit.identify()) 
      //     << "    isSCT=" << SiHitIdHelper::GetHelper()->isSCT(siHit.identify())
      //     << "    getBarrelEndcap=" << SiHitIdHelper::GetHelper()->getBarrelEndcap(siHit.identify())
      //     << "    getLayerDisk=" << SiHitIdHelper::GetHelper()->getLayerDisk(siHit.identify())
      //     << "    getEtaModule=" << SiHitIdHelper::GetHelper()->getEtaModule(siHit.identify())
      //     << "    getPhiModule=" << SiHitIdHelper::GetHelper()->getPhiModule(siHit.identify())
      //     << "    getSide=" << SiHitIdHelper::GetHelper()->getSide(siHit.identify());

      //cout << "SiHit::print" << endl;
      //siHit.print();

      siHits_energyLoss.push_back(siHit.energyLoss());
      siHits_meanTime.push_back(siHit.meanTime());
      siHits_localStartPositionX.push_back(siHit.localStartPosition().x() );
      siHits_localStartPositionY.push_back(siHit.localStartPosition().y());
      siHits_localStartPositionZ.push_back(siHit.localStartPosition().z());
      siHits_barrelEndcap.push_back(siHit.getBarrelEndcap());
      siHits_layerDisk.push_back(siHit.getLayerDisk());
      siHits_etaModule.push_back(siHit.getEtaModule());
      siHits_phiModule.push_back(siHit.getPhiModule());
      siHits_side.push_back(siHit.getSide());
      siHits_isPixel.push_back(siHit.isPixel());
      siHits_isSCT.push_back(siHit.isSCT());

      siHits_id.push_back(id_long);
      siHits_layer_disk.push_back(siHit.getLayerDisk());

      if (m_isGeometryLoaded) {
        siHits_eta_index.push_back(m_PixelID->eta_index(id));
        siHits_phi_index.push_back(m_PixelID->phi_index(id));
        siHits_is_blayer.push_back(m_PixelID->is_blayer(id));
        siHits_is_barrel.push_back(m_PixelID->is_barrel(id));
      
        // using the GeoSiHit does not work DURING the SIMULATION because the
        // pointer to the PixelDetectorManager object, InDetDD::PixelDetectorManager *_pix, is null, in the GeoSiHit::init function from GeoSiHit.icc
        // but WORKS during the reconstruction RDOtoESD
      
        GeoSiHit geoSiHit(siHit);       
        //HepGeom::Point3D<double> p = geoSiHit.getGlobalPosition();
        //cout << "GeoSiHit getGlobalPostion: " 
        //     << geoSiHit.getGlobalPosition().x() << " "
        //     << geoSiHit.getGlobalPosition().y() << " " 
        //     << geoSiHit.getGlobalPosition().z() << endl;
        siHits_globalPositionX.push_back(geoSiHit.getGlobalPosition().x());
        siHits_globalPositionY.push_back(geoSiHit.getGlobalPosition().y());
        siHits_globalPositionZ.push_back(geoSiHit.getGlobalPosition().z());
      
      }

    }    

    m_siHits_n.push_back( (siHits[i])->size() );
    m_siHits_id.push_back(siHits_id);
    m_siHits_energyLoss.push_back(siHits_energyLoss);
    m_siHits_meanTime.push_back(siHits_meanTime);
    m_siHits_localStartPositionX.push_back(siHits_localStartPositionX);
    m_siHits_localStartPositionY.push_back(siHits_localStartPositionY);
    m_siHits_localStartPositionZ.push_back(siHits_localStartPositionZ);
    m_siHits_globalPositionX.push_back(siHits_globalPositionX);
    m_siHits_globalPositionY.push_back(siHits_globalPositionY);
    m_siHits_globalPositionZ.push_back(siHits_globalPositionZ);
    m_siHits_barrelEndcap.push_back(siHits_barrelEndcap);
    m_siHits_layerDisk.push_back(siHits_layerDisk);
    m_siHits_etaModule.push_back(siHits_etaModule);
    m_siHits_phiModule.push_back(siHits_phiModule);
    m_siHits_side.push_back(siHits_side);
    m_siHits_eta_index.push_back(siHits_eta_index);
    m_siHits_phi_index.push_back(siHits_phi_index);
    m_siHits_is_barrel.push_back(siHits_is_barrel);
    m_siHits_layer_disk.push_back(siHits_layer_disk);
    m_siHits_is_blayer.push_back(siHits_is_blayer);
    m_siHits_isPixel.push_back(siHits_isPixel);
    m_siHits_isSCT.push_back(siHits_isSCT); 
  
  }

  //TRTUncompressedHits (TRT_HitCollection_p3) [InDet]

  //example of how to read the TRT hits: TrtHitsTestTool.cxx
  //typedef AtlasHitsVector<TRTUncompressedHit> TRTUncompressedHitCollection;
  //  AtlasHitsVector<T> => vector<T>
  /*const DataHandle<TRTUncompressedHitCollection> trtHits;
  if (evtStore()->retrieve(trtHits,"TRTUncompressedHits").isFailure()) {
    ATH_MSG_INFO ( "failed to retrieve the TRTUncompressedHitCollection" );
  } else {
    ATH_MSG_INFO ( "#TRTUncompresedHits=" << trtHits->size() );
    m_trtHits_n = trtHits->size();  
    }*/

  if (m_doLArHits) {

  m_larHits_n.clear();
  m_larHits_energy.clear();
  m_larHits_time.clear();
  m_larHits_CaloDetDescrElement_x.clear();
  m_larHits_CaloDetDescrElement_y.clear();
  m_larHits_CaloDetDescrElement_z.clear();
  m_larHits_samplingLayer.clear();

  ATH_MSG_INFO ("Reading the LAr containers");
  // LArHitMiniFCAl not present in the HITS files
  const TString LArHitContainers[4]={"LArHitEMB","LArHitEMEC","LArHitFCAL","LArHitHEC"}; 
  const LArHitContainer* larHits[4];
  for (int i=0;i<4;i++) {

    ATH_MSG_INFO(LArHitContainers[i]);
    if (evtStore()->retrieve(larHits[i], LArHitContainers[i].Data()).isFailure()) {
      ATH_MSG_INFO("Failed to read " << LArHitContainers[i] );
      return StatusCode::FAILURE; 
    }
      
    ATH_MSG_INFO ( "#" << LArHitContainers[i] << "=" << (larHits[i])->size() );

    vector<float> larHits_energy;
    vector<float> larHits_time;
    vector<float> larHits_x;
    vector<float> larHits_y;
    vector<float> larHits_z;
    vector<int> larHits_samplingLayer;
    for (const LArHit* larHit : *(larHits[i])) { // AthenaHitsVector<LArHit>
      larHits_energy.push_back(larHit->energy());
      larHits_time.push_back(larHit->time());

      if (m_isGeometryLoaded) {

      GeoLArHit geoLArHit(*larHit);
      larHits_x.push_back(geoLArHit.getDetDescrElement()->x());
      larHits_y.push_back(geoLArHit.getDetDescrElement()->y());
      larHits_z.push_back(geoLArHit.getDetDescrElement()->z());
      larHits_samplingLayer.push_back(geoLArHit.SamplingLayer());

      }
    }

    m_larHits_n.push_back(larHits[i]->size());
    m_larHits_energy.push_back(larHits_energy);
    m_larHits_time.push_back(larHits_time);
    m_larHits_CaloDetDescrElement_x.push_back(larHits_x);
    m_larHits_CaloDetDescrElement_y.push_back(larHits_y);
    m_larHits_CaloDetDescrElement_z.push_back(larHits_z);
    m_larHits_samplingLayer.push_back(larHits_samplingLayer);
  }

  }

  ATH_MSG_INFO("Reading the calibration hit containers");
  m_larCalibrationHits_n.clear();
  const TString CaloCalibrationHitContainers[6] = {
    "LArCalibrationHitActive", "LArCalibrationHitInactive", "LArCalibrationHitDeadMaterial",
    "TileCalibHitActiveCell", "TileCalibHitInactiveCell", "TileCalibHitDeadMaterial"
  };
  const CaloCalibrationHitContainer* caloCalibrationHits[6];
  for (int i=0;i<6;i++) 
    if (evtStore()->retrieve(caloCalibrationHits[i], CaloCalibrationHitContainers[i].Data()).isFailure()) 
      ATH_MSG_INFO ( "failed to retrieve " << CaloCalibrationHitContainers[i]);
    else {
      ATH_MSG_INFO ( "#" << CaloCalibrationHitContainers[i] << "=" << caloCalibrationHits[i]->size()); 
      m_larCalibrationHits_n.push_back( caloCalibrationHits[i]->size() );
    }

  ATH_MSG_INFO("Reading the Tile hit container");
  const TileHitVector* mbtsHits = 0;
  if (evtStore()->retrieve(mbtsHits,"MBTSHits").isFailure())
    ATH_MSG_INFO("Failed to read the MBTS HITS");
  else {
    ATH_MSG_DEBUG ( "#MBTS hits: " << mbtsHits->size() );
    m_mbtsHits_n = mbtsHits->Size();
  }
 
  const TileHitVector* tileHitVector = 0;
  if (evtStore()->retrieve(tileHitVector,"TileHitVec").isFailure())
    ATH_MSG_INFO("Failed to read the Tile HITS");
  else  {
    ATH_MSG_DEBUG ( "#tile hits: " << tileHitVector->Size() );
    m_tileHits_n = tileHitVector->Size();
  }

  //ATH_MSG_INFO("Reading the CSC hits");
  m_cscHits_globalTime.clear();
  m_cscHits_energyDeposit.clear();
  m_cscHits_kineticEnergy.clear();
  m_cscHits_trackNumber.clear();
  m_cscHits_globalPositionX.clear();
  m_cscHits_globalPositionY.clear();
  m_cscHits_globalPositionZ.clear();  
  const CSCSimHitCollection* cscHits = 0;
  if ( evtStore()->retrieve(cscHits, "CSC_Hits").isFailure() ) 
    ATH_MSG_INFO ( "failed to retrieve the CSC_Hits" );
  else {
    ATH_MSG_DEBUG ( "#CSC hits = " << cscHits->size() );

    CSCSimHitCollection::const_iterator cscIter = cscHits->begin();
    for (; cscIter!=cscHits->end(); cscIter++) {

      const CSCSimHit* cscHit = &(*cscIter);

      m_cscHits_globalTime.push_back( cscHit->globalTime() );
      m_cscHits_energyDeposit.push_back( cscHit->energyDeposit() );
      m_cscHits_kineticEnergy.push_back( cscHit->kineticEnergy() );
      m_cscHits_trackNumber.push_back( cscHit->trackNumber() );

      if (m_isGeometryLoaded) {
        GeoCSCHit geoCSCHit(*cscIter);
        m_cscHits_globalPositionX.push_back(geoCSCHit.getGlobalPosition().x());
        m_cscHits_globalPositionY.push_back(geoCSCHit.getGlobalPosition().y());
        m_cscHits_globalPositionZ.push_back(geoCSCHit.getGlobalPosition().z());
      }
    }
    m_cscHits_n = cscHits->size();
  }

  //ATH_MSG_INFO("Reading the MDT hits");
  m_mdtHits_globalTime.clear();
  m_mdtHits_driftRadius.clear();
  m_mdtHits_stepLength.clear();
  m_mdtHits_energyDeposit.clear();
  m_mdtHits_particleEncoding.clear();
  m_mdtHits_kineticEnergy.clear();
  m_mdtHits_meanTime.clear();
  m_mdtHits_trackNumber.clear();
  m_mdtHits_globalPositionX.clear();
  m_mdtHits_globalPositionY.clear();
  m_mdtHits_globalPositionZ.clear();  
  const MDTSimHitCollection* mdtHits = 0;
  if ( evtStore()->retrieve(mdtHits, "MDT_Hits").isFailure() ) 
    ATH_MSG_INFO ( "failed to retrieve the MDT_Hits" );
  else {
    ATH_MSG_INFO ( "#MDT_Hits=" << mdtHits->size() );
    m_mdtHits_n = mdtHits->size();

    MDTSimHitCollection::const_iterator mdtIter = mdtHits->begin();
    for (; mdtIter!=mdtHits->end(); mdtIter++) {

      const MDTSimHit* mdtHit = &(*mdtIter); // like in MuonSpectrometer/MuonValidation/MuonPRDTest/src/MDTPRDValAlg.cxx
    
      m_mdtHits_globalTime.push_back( mdtHit->globalTime() );
      m_mdtHits_driftRadius.push_back( mdtHit->driftRadius() );
      m_mdtHits_stepLength.push_back( mdtHit->stepLength() );
      m_mdtHits_energyDeposit.push_back( mdtHit->energyDeposit() );
      m_mdtHits_particleEncoding.push_back( mdtHit->particleEncoding() );
      m_mdtHits_kineticEnergy.push_back( mdtHit->kineticEnergy() );
      m_mdtHits_meanTime.push_back( mdtHit->meanTime() );
      m_mdtHits_trackNumber.push_back( mdtHit->trackNumber() );

      if (m_isGeometryLoaded) {
        GeoMDTHit geoMDTHit(*mdtIter);
        m_mdtHits_globalPositionX.push_back(geoMDTHit.getGlobalPosition().x());
        m_mdtHits_globalPositionY.push_back(geoMDTHit.getGlobalPosition().y());
        m_mdtHits_globalPositionZ.push_back(geoMDTHit.getGlobalPosition().z());
      }
    }


  }

  ATH_MSG_INFO("Reading the TGC hits");
  const TGCSimHitCollection* tgcHits = 0;
  if ( evtStore()->retrieve(tgcHits, "TGC_Hits").isFailure() ) 
    ATH_MSG_INFO ( "failed to retrieve the TGC_Hits" );
  else {
    ATH_MSG_DEBUG ( "#TGC_Hits=" << tgcHits->size() );
    m_tgcHits_n = tgcHits->size();
  }

  ATH_MSG_INFO("Reading the RPC hits");
  const RPCSimHitCollection* rpcHits = 0;
  if ( evtStore()->retrieve(rpcHits, "RPC_Hits").isFailure() ) 
    ATH_MSG_INFO ( "failed to retrieve the RPC_Hits" );
  else {
    ATH_MSG_DEBUG ( "#RPC_Hits=" << rpcHits->size() );
    m_rpcHits_n = rpcHits->size();
  }

  ATH_MSG_INFO("Reading the RecoTimingObj");
  const RecoTimingObj* recoTimingObj = 0;
  if (evtStore()->retrieve(recoTimingObj, "EVNTtoHITS_timings").isFailure()) 
    ATH_MSG_INFO( "failed to retrieve the RecoTimingObj ");
  else {
    ATH_MSG_DEBUG ( "RecoTimingObj size=" << recoTimingObj->size() ); 
   for (unsigned int i=0;i<recoTimingObj->size();i++)
     ATH_MSG_DEBUG ( "word #" << i << " " << (*recoTimingObj)[i] );
  }

  //const TrackRecordCollection_p2* caloEntryLayer = 0;
  //if (evtStore()->retrieve(caloEntryLayer, "CaloEntryLayer").isFailure())
  //  ATH_MSG_INFO ("failed to retrieve theTrackRecordCollection with key CaloEntryLayer");
  //else {

  //}
  /*
  m_pixelClusters_omegax.clear();
  m_pixelClusters_omegay.clear();
  m_pixelClusters_totalCharge.clear();
  m_pixelClusters_energyLoss.clear();  
  m_pixelClusters_isFake.clear();
  m_pixelClusters_isAmbiguous.clear();
  m_pixelClusters_isSplit.clear();

  const InDet::PixelClusterContainer* pixelClusterContainer = 0;
  if (evtStore()->retrieve(pixelClusterContainer, "PixelClusterContainer").isFailure())
    ATH_MSG_INFO ("Failed to retrieve the PixelClusterContainer");
  else {
    int colNum = 0;
    for (InDet::PixelClusterContainer::const_iterator col = pixelClusterContainer->begin();
          col!= pixelClusterContainer->end(); col++) {
      ATH_MSG_INFO("PixelClusterContainer collection #" << colNum);
      colNum++;

      for (InDet::PixelClusterCollection::const_iterator clus = (*col)->begin();
          clus!=(*col)->end();clus++) {
        ATH_MSG_INFO ("  PixelCluster omegax=" << (*clus)->omegax() 
                                 << " omegay=" << (*clus)->omegay()
                                 << " energyLoss=" << (*clus)->energyLoss() 
                                 << " isFake=" << (*clus)->isFake()
                                 << " isAmbiguous=" << (*clus)->isAmbiguous()
                                 << " isSplit=" << (*clus)->isSplit()
                     );
        m_pixelClusters_omegax.push_back((*clus)->omegax());
        m_pixelClusters_omegay.push_back((*clus)->omegay()); 
        m_pixelClusters_energyLoss.push_back((*clus)->energyLoss()); 
        m_pixelClusters_totalCharge.push_back((*clus)->totalCharge()); 
        m_pixelClusters_isFake.push_back((*clus)->isFake());
        m_pixelClusters_isAmbiguous.push_back((*clus)->isAmbiguous());
        m_pixelClusters_isSplit.push_back((*clus)->isSplit());
      }
    }

  }*/

  m_tree->Fill();

  ATH_MSG_INFO("m_tree->GetEntries()=" << m_tree->GetEntries());

  return StatusCode::SUCCESS;
}

StatusCode read_HITSAlg::beginInputFile() {  
  //example of metadata retrieval:
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );

  return StatusCode::SUCCESS;
}



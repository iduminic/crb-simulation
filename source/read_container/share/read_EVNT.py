## get a handle to the ServiceManager
from AthenaCommon.AppMgr import ServiceMgr

## get a handle to the ApplicationManager
from AthenaCommon.AppMgr import theApp
theApp.EvtMax=1                                         #says how many events to run over. Set to -1 for all event

import os

import AthenaPoolCnvSvc.ReadAthenaPool                   #sets up reading of POOL files (e.g. xAODs)
svcMgr.EventSelector.InputCollections = [ "/storage/baaf/backup/olariu/fluka/EVGEN/ir1_BG_bs_6500GeV_b1_20MeV_nprim3198000_67.pzLimits10000MeV.rLimits0to12000mm.evgen.root"]   #insert your list of input files here

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from read_container.read_containerConf import read_EVNTAlg
#from read_EVNTConf import read_EVNTAlg
topSequence+=read_EVNTAlg()

include("AthAnalysisBaseComps/SuppressLogging.py")       #Optional include to suppress as much athena output as possible


# This won't work due to a crash when reading  the LAr HIT containers.
# This is traced to the calo cell id manager which is not initialized properly (null pointer) => CaloIdentifier package, CaloIdManager.cxx
# This guy also had this problem: https://groups.cern.ch/group/atlas-simulation/Lists/Archive/Flat.aspx?RootFolder=%2fgroup%2fatlas-simulation%2fLists%2fArchive%2fAccessing%20LAr%20containers%20in%20HITS%20files&FolderCTID=0x01200200ECB55EE754BB494DAEF0F9693C514D0B
# but no solution.
# So the solution is to run this algorithm during the digitization, that worked.

## get a handle to the ServiceManager
from AthenaCommon.AppMgr import ServiceMgr

## get a handle to the ApplicationManager
from AthenaCommon.AppMgr import theApp

theApp.EvtMax = -1

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from read_container.read_containerConf import read_HITSAlg
#from read_HITSConf import read_HITSAlg
topSequence+=read_HITSAlg()

import AthenaPoolCnvSvc.ReadAthenaPool

import os

#svcMgr.MessageSvc.OutputLevel = INFO
svcMgr.MessageSvc.OutputLevel = DEBUG
svcMgr.MessageSvc.infoLimit = 0
svcMgr.EventSelector.InputCollections = [ "HITS.04919495._001041.pool.root.1" ]
#svcMgr.EventSelector.InputCollections = [ "../../G4Atlas/HITS/ir1_BG_bs_6500GeV_b1_20MeV_nprim3198000_67.pzLimits10000MeV.rLimits0to12000mm.313061.G4Atlas.EvtMax100.HITS.root" ]
#svcMgr.EventSelector.InputCollections = [ os.environ['STORAGE'] + "/fluka/HITS/ir1_BG_bs_6500GeV_b1_20MeV_nprim3198000_67.pzLimits10000MeV.rLimits0to12000mm.313061.G4Atlas.EvtMax10000.HITS.root" ]

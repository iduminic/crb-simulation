# echo "cleanup read_container read_container-00-00-00 in /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.7.5/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtread_containertempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtread_containertempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=read_container -version=read_container-00-00-00 -path=/data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf  $* >${cmtread_containertempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=read_container -version=read_container-00-00-00 -path=/data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf  $* >${cmtread_containertempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtread_containertempfile}
  unset cmtread_containertempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtread_containertempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtread_containertempfile}
unset cmtread_containertempfile
return $cmtcleanupstatus


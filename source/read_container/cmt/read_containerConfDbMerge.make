#-- start of make_header -----------------

#====================================
#  Document read_containerConfDbMerge
#
#   Generated Tue Apr 11 18:10:18 2017  by olariu
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_read_containerConfDbMerge_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_read_containerConfDbMerge_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_read_containerConfDbMerge

read_container_tag = $(tag)

#cmt_local_tagfile_read_containerConfDbMerge = $(read_container_tag)_read_containerConfDbMerge.make
cmt_local_tagfile_read_containerConfDbMerge = $(bin)$(read_container_tag)_read_containerConfDbMerge.make

else

tags      = $(tag),$(CMTEXTRATAGS)

read_container_tag = $(tag)

#cmt_local_tagfile_read_containerConfDbMerge = $(read_container_tag).make
cmt_local_tagfile_read_containerConfDbMerge = $(bin)$(read_container_tag).make

endif

include $(cmt_local_tagfile_read_containerConfDbMerge)
#-include $(cmt_local_tagfile_read_containerConfDbMerge)

ifdef cmt_read_containerConfDbMerge_has_target_tag

cmt_final_setup_read_containerConfDbMerge = $(bin)setup_read_containerConfDbMerge.make
cmt_dependencies_in_read_containerConfDbMerge = $(bin)dependencies_read_containerConfDbMerge.in
#cmt_final_setup_read_containerConfDbMerge = $(bin)read_container_read_containerConfDbMergesetup.make
cmt_local_read_containerConfDbMerge_makefile = $(bin)read_containerConfDbMerge.make

else

cmt_final_setup_read_containerConfDbMerge = $(bin)setup.make
cmt_dependencies_in_read_containerConfDbMerge = $(bin)dependencies.in
#cmt_final_setup_read_containerConfDbMerge = $(bin)read_containersetup.make
cmt_local_read_containerConfDbMerge_makefile = $(bin)read_containerConfDbMerge.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)read_containersetup.make

#read_containerConfDbMerge :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'read_containerConfDbMerge'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = read_containerConfDbMerge/
#read_containerConfDbMerge::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_genconfDb_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.confdb file into a single
# <project>.confdb file in the (lib) install area

.PHONY: read_containerConfDbMerge read_containerConfDbMergeclean

# default is already '#'
#genconfDb_comment_char := "'#'"

instdir      := ${CMTINSTALLAREA}/$(tag)
confDbRef    := /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/read_container/genConf/read_container/read_container.confdb
stampConfDb  := $(confDbRef).stamp
mergedConfDb := $(instdir)/lib/$(project).confdb

read_containerConfDbMerge :: $(stampConfDb) $(mergedConfDb)
	@:

.NOTPARALLEL : $(stampConfDb) $(mergedConfDb)

$(stampConfDb) $(mergedConfDb) :: $(confDbRef)
	@echo "Running merge_genconfDb  read_containerConfDbMerge"
	$(merge_genconfDb_cmd) \
          --do-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)

read_containerConfDbMergeclean ::
	$(cleanup_silent) $(merge_genconfDb_cmd) \
          --un-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)	;
	$(cleanup_silent) $(remove_command) $(stampConfDb)
libread_container_so_dependencies = ../x86_64-slc6-gcc49-opt/libread_container.so
#-- start of cleanup_header --------------

clean :: read_containerConfDbMergeclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(read_containerConfDbMerge.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

read_containerConfDbMergeclean ::
#-- end of cleanup_header ---------------

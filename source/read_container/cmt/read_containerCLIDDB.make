#-- start of make_header -----------------

#====================================
#  Document read_containerCLIDDB
#
#   Generated Tue Apr 11 18:10:16 2017  by olariu
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_read_containerCLIDDB_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_read_containerCLIDDB_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_read_containerCLIDDB

read_container_tag = $(tag)

#cmt_local_tagfile_read_containerCLIDDB = $(read_container_tag)_read_containerCLIDDB.make
cmt_local_tagfile_read_containerCLIDDB = $(bin)$(read_container_tag)_read_containerCLIDDB.make

else

tags      = $(tag),$(CMTEXTRATAGS)

read_container_tag = $(tag)

#cmt_local_tagfile_read_containerCLIDDB = $(read_container_tag).make
cmt_local_tagfile_read_containerCLIDDB = $(bin)$(read_container_tag).make

endif

include $(cmt_local_tagfile_read_containerCLIDDB)
#-include $(cmt_local_tagfile_read_containerCLIDDB)

ifdef cmt_read_containerCLIDDB_has_target_tag

cmt_final_setup_read_containerCLIDDB = $(bin)setup_read_containerCLIDDB.make
cmt_dependencies_in_read_containerCLIDDB = $(bin)dependencies_read_containerCLIDDB.in
#cmt_final_setup_read_containerCLIDDB = $(bin)read_container_read_containerCLIDDBsetup.make
cmt_local_read_containerCLIDDB_makefile = $(bin)read_containerCLIDDB.make

else

cmt_final_setup_read_containerCLIDDB = $(bin)setup.make
cmt_dependencies_in_read_containerCLIDDB = $(bin)dependencies.in
#cmt_final_setup_read_containerCLIDDB = $(bin)read_containersetup.make
cmt_local_read_containerCLIDDB_makefile = $(bin)read_containerCLIDDB.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)read_containersetup.make

#read_containerCLIDDB :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'read_containerCLIDDB'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = read_containerCLIDDB/
#read_containerCLIDDB::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genCLIDDB_header
# Author: Paolo Calafiura
# derived from genconf_header

# Use genCLIDDB_cmd to create package clid.db files

.PHONY: read_containerCLIDDB read_containerCLIDDBclean

outname := clid.db
cliddb  := read_container_$(outname)
instdir := $(CMTINSTALLAREA)/share
result  := $(instdir)/$(cliddb)
product := $(instdir)/$(outname)
conflib := $(bin)$(library_prefix)read_container.$(shlibsuffix)

read_containerCLIDDB :: $(result)

$(instdir) :
	$(mkdir) -p $(instdir)

$(result) : $(conflib) $(product)
	@$(genCLIDDB_cmd) -p read_container -i$(product) -o $(result)

$(product) : $(instdir)
	touch $(product)

read_containerCLIDDBclean ::
	$(cleanup_silent) $(uninstall_command) $(product) $(result)
	$(cleanup_silent) $(cmt_uninstallarea_command) $(product) $(result)

#-- start of cleanup_header --------------

clean :: read_containerCLIDDBclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(read_containerCLIDDB.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

read_containerCLIDDBclean ::
#-- end of cleanup_header ---------------

#-- start of make_header -----------------

#====================================
#  Document read_containerComponentsList
#
#   Generated Tue Apr 11 18:10:19 2017  by olariu
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_read_containerComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_read_containerComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_read_containerComponentsList

read_container_tag = $(tag)

#cmt_local_tagfile_read_containerComponentsList = $(read_container_tag)_read_containerComponentsList.make
cmt_local_tagfile_read_containerComponentsList = $(bin)$(read_container_tag)_read_containerComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

read_container_tag = $(tag)

#cmt_local_tagfile_read_containerComponentsList = $(read_container_tag).make
cmt_local_tagfile_read_containerComponentsList = $(bin)$(read_container_tag).make

endif

include $(cmt_local_tagfile_read_containerComponentsList)
#-include $(cmt_local_tagfile_read_containerComponentsList)

ifdef cmt_read_containerComponentsList_has_target_tag

cmt_final_setup_read_containerComponentsList = $(bin)setup_read_containerComponentsList.make
cmt_dependencies_in_read_containerComponentsList = $(bin)dependencies_read_containerComponentsList.in
#cmt_final_setup_read_containerComponentsList = $(bin)read_container_read_containerComponentsListsetup.make
cmt_local_read_containerComponentsList_makefile = $(bin)read_containerComponentsList.make

else

cmt_final_setup_read_containerComponentsList = $(bin)setup.make
cmt_dependencies_in_read_containerComponentsList = $(bin)dependencies.in
#cmt_final_setup_read_containerComponentsList = $(bin)read_containersetup.make
cmt_local_read_containerComponentsList_makefile = $(bin)read_containerComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)read_containersetup.make

#read_containerComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'read_containerComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = read_containerComponentsList/
#read_containerComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
##
componentslistfile = read_container.components
COMPONENTSLIST_DIR = ../$(tag)
fulllibname = libread_container.$(shlibsuffix)

read_containerComponentsList :: ${COMPONENTSLIST_DIR}/$(componentslistfile)
	@:

${COMPONENTSLIST_DIR}/$(componentslistfile) :: $(bin)$(fulllibname)
	@echo 'Generating componentslist file for $(fulllibname)'
	cd ../$(tag);$(listcomponents_cmd) --output ${COMPONENTSLIST_DIR}/$(componentslistfile) $(fulllibname)

install :: read_containerComponentsListinstall
read_containerComponentsListinstall :: read_containerComponentsList

uninstall :: read_containerComponentsListuninstall
read_containerComponentsListuninstall :: read_containerComponentsListclean

read_containerComponentsListclean ::
	@echo 'Deleting $(componentslistfile)'
	@rm -f ${COMPONENTSLIST_DIR}/$(componentslistfile)

#-- start of cleanup_header --------------

clean :: read_containerComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(read_containerComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

read_containerComponentsListclean ::
#-- end of cleanup_header ---------------

#-- start of make_header -----------------

#====================================
#  Document read_containerConf
#
#   Generated Tue Apr 11 18:10:16 2017  by olariu
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_read_containerConf_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_read_containerConf_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_read_containerConf

read_container_tag = $(tag)

#cmt_local_tagfile_read_containerConf = $(read_container_tag)_read_containerConf.make
cmt_local_tagfile_read_containerConf = $(bin)$(read_container_tag)_read_containerConf.make

else

tags      = $(tag),$(CMTEXTRATAGS)

read_container_tag = $(tag)

#cmt_local_tagfile_read_containerConf = $(read_container_tag).make
cmt_local_tagfile_read_containerConf = $(bin)$(read_container_tag).make

endif

include $(cmt_local_tagfile_read_containerConf)
#-include $(cmt_local_tagfile_read_containerConf)

ifdef cmt_read_containerConf_has_target_tag

cmt_final_setup_read_containerConf = $(bin)setup_read_containerConf.make
cmt_dependencies_in_read_containerConf = $(bin)dependencies_read_containerConf.in
#cmt_final_setup_read_containerConf = $(bin)read_container_read_containerConfsetup.make
cmt_local_read_containerConf_makefile = $(bin)read_containerConf.make

else

cmt_final_setup_read_containerConf = $(bin)setup.make
cmt_dependencies_in_read_containerConf = $(bin)dependencies.in
#cmt_final_setup_read_containerConf = $(bin)read_containersetup.make
cmt_local_read_containerConf_makefile = $(bin)read_containerConf.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)read_containersetup.make

#read_containerConf :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'read_containerConf'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = read_containerConf/
#read_containerConf::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genconfig_header
# Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

# Use genconf.exe to create configurables python modules, then have the
# normal python install procedure take over.

.PHONY: read_containerConf read_containerConfclean

confpy  := read_containerConf.py
conflib := $(bin)$(library_prefix)read_container.$(shlibsuffix)
confdb  := read_container.confdb
instdir := $(CMTINSTALLAREA)$(shared_install_subdir)/python/$(package)
product := $(instdir)/$(confpy)
initpy  := $(instdir)/__init__.py

ifdef GENCONF_ECHO
genconf_silent =
else
genconf_silent = $(silent)
endif

read_containerConf :: read_containerConfinstall

install :: read_containerConfinstall

read_containerConfinstall : /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/read_container/genConf/read_container/$(confpy)
	@echo "Installing /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/read_container/genConf/read_container in /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/InstallArea/python" ; \
	 $(install_command) --exclude="*.py?" --exclude="__init__.py" --exclude="*.confdb" /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/read_container/genConf/read_container /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/InstallArea/python ; \

/data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/read_container/genConf/read_container/$(confpy) : $(conflib) /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/read_container/genConf/read_container
	$(genconf_silent) $(genconfig_cmd)   -o /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/read_container/genConf/read_container -p $(package) \
	  --configurable-module=GaudiKernel.Proxy \
	  --configurable-default-name=Configurable.DefaultName \
	  --configurable-algorithm=ConfigurableAlgorithm \
	  --configurable-algtool=ConfigurableAlgTool \
	  --configurable-auditor=ConfigurableAuditor \
          --configurable-service=ConfigurableService \
	  -i ../$(tag)/$(library_prefix)read_container.$(shlibsuffix)

/data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/read_container/genConf/read_container:
	@ if [ ! -d /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/read_container/genConf/read_container ] ; then mkdir -p /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/read_container/genConf/read_container ; fi ;

read_containerConfclean :: read_containerConfuninstall
	$(cleanup_silent) $(remove_command) /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/read_container/genConf/read_container/$(confpy) /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/read_container/genConf/read_container/$(confdb)

uninstall :: read_containerConfuninstall

read_containerConfuninstall ::
	@$(uninstall_command) /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf/InstallArea/python
libread_container_so_dependencies = ../x86_64-slc6-gcc49-opt/libread_container.so
#-- start of cleanup_header --------------

clean :: read_containerConfclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(read_containerConf.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

read_containerConfclean ::
#-- end of cleanup_header ---------------

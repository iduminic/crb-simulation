#-- start of make_header -----------------

#====================================
#  Library read_container
#
#   Generated Tue Apr 11 18:09:53 2017  by olariu
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_read_container_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_read_container_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_read_container

read_container_tag = $(tag)

#cmt_local_tagfile_read_container = $(read_container_tag)_read_container.make
cmt_local_tagfile_read_container = $(bin)$(read_container_tag)_read_container.make

else

tags      = $(tag),$(CMTEXTRATAGS)

read_container_tag = $(tag)

#cmt_local_tagfile_read_container = $(read_container_tag).make
cmt_local_tagfile_read_container = $(bin)$(read_container_tag).make

endif

include $(cmt_local_tagfile_read_container)
#-include $(cmt_local_tagfile_read_container)

ifdef cmt_read_container_has_target_tag

cmt_final_setup_read_container = $(bin)setup_read_container.make
cmt_dependencies_in_read_container = $(bin)dependencies_read_container.in
#cmt_final_setup_read_container = $(bin)read_container_read_containersetup.make
cmt_local_read_container_makefile = $(bin)read_container.make

else

cmt_final_setup_read_container = $(bin)setup.make
cmt_dependencies_in_read_container = $(bin)dependencies.in
#cmt_final_setup_read_container = $(bin)read_containersetup.make
cmt_local_read_container_makefile = $(bin)read_container.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)read_containersetup.make

#read_container :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'read_container'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = read_container/
#read_container::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

read_containerlibname   = $(bin)$(library_prefix)read_container$(library_suffix)
read_containerlib       = $(read_containerlibname).a
read_containerstamp     = $(bin)read_container.stamp
read_containershstamp   = $(bin)read_container.shstamp

read_container :: dirs  read_containerLIB
	$(echo) "read_container ok"

#-- end of libary_header ----------------
#-- start of library_no_static ------

#read_containerLIB :: $(read_containerlib) $(read_containershstamp)
read_containerLIB :: $(read_containershstamp)
	$(echo) "read_container : library ok"

$(read_containerlib) :: $(bin)read_TRUTH.o $(bin)SiClusterReader.o $(bin)PixelRDOReader.o $(bin)SiSpacePointsReaderAlg.o $(bin)read_HITSAlg.o $(bin)PixelConditionsSummaryReader.o $(bin)read_container_load.o $(bin)read_container_entries.o
	$(lib_echo) "static library $@"
	$(lib_silent) cd $(bin); \
	  $(ar) $(read_containerlib) $?
	$(lib_silent) $(ranlib) $(read_containerlib)
	$(lib_silent) cat /dev/null >$(read_containerstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

#
# We add one level of dependency upon the true shared library 
# (rather than simply upon the stamp file)
# this is for cases where the shared library has not been built
# while the stamp was created (error??) 
#

$(read_containerlibname).$(shlibsuffix) :: $(bin)read_TRUTH.o $(bin)SiClusterReader.o $(bin)PixelRDOReader.o $(bin)SiSpacePointsReaderAlg.o $(bin)read_HITSAlg.o $(bin)PixelConditionsSummaryReader.o $(bin)read_container_load.o $(bin)read_container_entries.o $(use_requirements) $(read_containerstamps)
	$(lib_echo) "shared library $@"
	$(lib_silent) $(shlibbuilder) $(shlibflags) -o $@ $(bin)read_TRUTH.o $(bin)SiClusterReader.o $(bin)PixelRDOReader.o $(bin)SiSpacePointsReaderAlg.o $(bin)read_HITSAlg.o $(bin)PixelConditionsSummaryReader.o $(bin)read_container_load.o $(bin)read_container_entries.o $(read_container_shlibflags)
	$(lib_silent) cat /dev/null >$(read_containerstamp) && \
	  cat /dev/null >$(read_containershstamp)

$(read_containershstamp) :: $(read_containerlibname).$(shlibsuffix)
	$(lib_silent) if test -f $(read_containerlibname).$(shlibsuffix) ; then \
	  cat /dev/null >$(read_containerstamp) && \
	  cat /dev/null >$(read_containershstamp) ; fi

read_containerclean ::
	$(cleanup_echo) objects read_container
	$(cleanup_silent) /bin/rm -f $(bin)read_TRUTH.o $(bin)SiClusterReader.o $(bin)PixelRDOReader.o $(bin)SiSpacePointsReaderAlg.o $(bin)read_HITSAlg.o $(bin)PixelConditionsSummaryReader.o $(bin)read_container_load.o $(bin)read_container_entries.o
	$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)read_TRUTH.o $(bin)SiClusterReader.o $(bin)PixelRDOReader.o $(bin)SiSpacePointsReaderAlg.o $(bin)read_HITSAlg.o $(bin)PixelConditionsSummaryReader.o $(bin)read_container_load.o $(bin)read_container_entries.o) $(patsubst %.o,%.dep,$(bin)read_TRUTH.o $(bin)SiClusterReader.o $(bin)PixelRDOReader.o $(bin)SiSpacePointsReaderAlg.o $(bin)read_HITSAlg.o $(bin)PixelConditionsSummaryReader.o $(bin)read_container_load.o $(bin)read_container_entries.o) $(patsubst %.o,%.d.stamp,$(bin)read_TRUTH.o $(bin)SiClusterReader.o $(bin)PixelRDOReader.o $(bin)SiSpacePointsReaderAlg.o $(bin)read_HITSAlg.o $(bin)PixelConditionsSummaryReader.o $(bin)read_container_load.o $(bin)read_container_entries.o)
	$(cleanup_silent) cd $(bin); /bin/rm -rf read_container_deps read_container_dependencies.make

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
read_containerinstallname = $(library_prefix)read_container$(library_suffix).$(shlibsuffix)

read_container :: read_containerinstall ;

install :: read_containerinstall ;

read_containerinstall :: $(install_dir)/$(read_containerinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(read_containerinstallname) :: $(bin)$(read_containerinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(read_containerinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##read_containerclean :: read_containeruninstall

uninstall :: read_containeruninstall ;

read_containeruninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(read_containerinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- end of library_no_static ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),read_containerclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)read_TRUTH.d

$(bin)$(binobj)read_TRUTH.d :

$(bin)$(binobj)read_TRUTH.o : $(cmt_final_setup_read_container)

$(bin)$(binobj)read_TRUTH.o : $(src)read_TRUTH.cxx
	$(cpp_echo) $(src)read_TRUTH.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(read_TRUTH_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(read_TRUTH_cppflags) $(read_TRUTH_cxx_cppflags)  $(src)read_TRUTH.cxx
endif
endif

else
$(bin)read_container_dependencies.make : $(read_TRUTH_cxx_dependencies)

$(bin)read_container_dependencies.make : $(src)read_TRUTH.cxx

$(bin)$(binobj)read_TRUTH.o : $(read_TRUTH_cxx_dependencies)
	$(cpp_echo) $(src)read_TRUTH.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(read_TRUTH_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(read_TRUTH_cppflags) $(read_TRUTH_cxx_cppflags)  $(src)read_TRUTH.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),read_containerclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)SiClusterReader.d

$(bin)$(binobj)SiClusterReader.d :

$(bin)$(binobj)SiClusterReader.o : $(cmt_final_setup_read_container)

$(bin)$(binobj)SiClusterReader.o : $(src)SiClusterReader.cxx
	$(cpp_echo) $(src)SiClusterReader.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(SiClusterReader_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(SiClusterReader_cppflags) $(SiClusterReader_cxx_cppflags)  $(src)SiClusterReader.cxx
endif
endif

else
$(bin)read_container_dependencies.make : $(SiClusterReader_cxx_dependencies)

$(bin)read_container_dependencies.make : $(src)SiClusterReader.cxx

$(bin)$(binobj)SiClusterReader.o : $(SiClusterReader_cxx_dependencies)
	$(cpp_echo) $(src)SiClusterReader.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(SiClusterReader_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(SiClusterReader_cppflags) $(SiClusterReader_cxx_cppflags)  $(src)SiClusterReader.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),read_containerclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)PixelRDOReader.d

$(bin)$(binobj)PixelRDOReader.d :

$(bin)$(binobj)PixelRDOReader.o : $(cmt_final_setup_read_container)

$(bin)$(binobj)PixelRDOReader.o : $(src)PixelRDOReader.cxx
	$(cpp_echo) $(src)PixelRDOReader.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(PixelRDOReader_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(PixelRDOReader_cppflags) $(PixelRDOReader_cxx_cppflags)  $(src)PixelRDOReader.cxx
endif
endif

else
$(bin)read_container_dependencies.make : $(PixelRDOReader_cxx_dependencies)

$(bin)read_container_dependencies.make : $(src)PixelRDOReader.cxx

$(bin)$(binobj)PixelRDOReader.o : $(PixelRDOReader_cxx_dependencies)
	$(cpp_echo) $(src)PixelRDOReader.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(PixelRDOReader_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(PixelRDOReader_cppflags) $(PixelRDOReader_cxx_cppflags)  $(src)PixelRDOReader.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),read_containerclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)SiSpacePointsReaderAlg.d

$(bin)$(binobj)SiSpacePointsReaderAlg.d :

$(bin)$(binobj)SiSpacePointsReaderAlg.o : $(cmt_final_setup_read_container)

$(bin)$(binobj)SiSpacePointsReaderAlg.o : $(src)SiSpacePointsReaderAlg.cxx
	$(cpp_echo) $(src)SiSpacePointsReaderAlg.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(SiSpacePointsReaderAlg_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(SiSpacePointsReaderAlg_cppflags) $(SiSpacePointsReaderAlg_cxx_cppflags)  $(src)SiSpacePointsReaderAlg.cxx
endif
endif

else
$(bin)read_container_dependencies.make : $(SiSpacePointsReaderAlg_cxx_dependencies)

$(bin)read_container_dependencies.make : $(src)SiSpacePointsReaderAlg.cxx

$(bin)$(binobj)SiSpacePointsReaderAlg.o : $(SiSpacePointsReaderAlg_cxx_dependencies)
	$(cpp_echo) $(src)SiSpacePointsReaderAlg.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(SiSpacePointsReaderAlg_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(SiSpacePointsReaderAlg_cppflags) $(SiSpacePointsReaderAlg_cxx_cppflags)  $(src)SiSpacePointsReaderAlg.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),read_containerclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)read_HITSAlg.d

$(bin)$(binobj)read_HITSAlg.d :

$(bin)$(binobj)read_HITSAlg.o : $(cmt_final_setup_read_container)

$(bin)$(binobj)read_HITSAlg.o : $(src)read_HITSAlg.cxx
	$(cpp_echo) $(src)read_HITSAlg.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(read_HITSAlg_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(read_HITSAlg_cppflags) $(read_HITSAlg_cxx_cppflags)  $(src)read_HITSAlg.cxx
endif
endif

else
$(bin)read_container_dependencies.make : $(read_HITSAlg_cxx_dependencies)

$(bin)read_container_dependencies.make : $(src)read_HITSAlg.cxx

$(bin)$(binobj)read_HITSAlg.o : $(read_HITSAlg_cxx_dependencies)
	$(cpp_echo) $(src)read_HITSAlg.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(read_HITSAlg_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(read_HITSAlg_cppflags) $(read_HITSAlg_cxx_cppflags)  $(src)read_HITSAlg.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),read_containerclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)PixelConditionsSummaryReader.d

$(bin)$(binobj)PixelConditionsSummaryReader.d :

$(bin)$(binobj)PixelConditionsSummaryReader.o : $(cmt_final_setup_read_container)

$(bin)$(binobj)PixelConditionsSummaryReader.o : $(src)PixelConditionsSummaryReader.cxx
	$(cpp_echo) $(src)PixelConditionsSummaryReader.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(PixelConditionsSummaryReader_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(PixelConditionsSummaryReader_cppflags) $(PixelConditionsSummaryReader_cxx_cppflags)  $(src)PixelConditionsSummaryReader.cxx
endif
endif

else
$(bin)read_container_dependencies.make : $(PixelConditionsSummaryReader_cxx_dependencies)

$(bin)read_container_dependencies.make : $(src)PixelConditionsSummaryReader.cxx

$(bin)$(binobj)PixelConditionsSummaryReader.o : $(PixelConditionsSummaryReader_cxx_dependencies)
	$(cpp_echo) $(src)PixelConditionsSummaryReader.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(PixelConditionsSummaryReader_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(PixelConditionsSummaryReader_cppflags) $(PixelConditionsSummaryReader_cxx_cppflags)  $(src)PixelConditionsSummaryReader.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),read_containerclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)read_container_load.d

$(bin)$(binobj)read_container_load.d :

$(bin)$(binobj)read_container_load.o : $(cmt_final_setup_read_container)

$(bin)$(binobj)read_container_load.o : $(src)components/read_container_load.cxx
	$(cpp_echo) $(src)components/read_container_load.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(read_container_load_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(read_container_load_cppflags) $(read_container_load_cxx_cppflags) -I../src/components $(src)components/read_container_load.cxx
endif
endif

else
$(bin)read_container_dependencies.make : $(read_container_load_cxx_dependencies)

$(bin)read_container_dependencies.make : $(src)components/read_container_load.cxx

$(bin)$(binobj)read_container_load.o : $(read_container_load_cxx_dependencies)
	$(cpp_echo) $(src)components/read_container_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(read_container_load_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(read_container_load_cppflags) $(read_container_load_cxx_cppflags) -I../src/components $(src)components/read_container_load.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),read_containerclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)read_container_entries.d

$(bin)$(binobj)read_container_entries.d :

$(bin)$(binobj)read_container_entries.o : $(cmt_final_setup_read_container)

$(bin)$(binobj)read_container_entries.o : $(src)components/read_container_entries.cxx
	$(cpp_echo) $(src)components/read_container_entries.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(read_container_entries_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(read_container_entries_cppflags) $(read_container_entries_cxx_cppflags) -I../src/components $(src)components/read_container_entries.cxx
endif
endif

else
$(bin)read_container_dependencies.make : $(read_container_entries_cxx_dependencies)

$(bin)read_container_dependencies.make : $(src)components/read_container_entries.cxx

$(bin)$(binobj)read_container_entries.o : $(read_container_entries_cxx_dependencies)
	$(cpp_echo) $(src)components/read_container_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(read_container_pp_cppflags) $(lib_read_container_pp_cppflags) $(read_container_entries_pp_cppflags) $(use_cppflags) $(read_container_cppflags) $(lib_read_container_cppflags) $(read_container_entries_cppflags) $(read_container_entries_cxx_cppflags) -I../src/components $(src)components/read_container_entries.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: read_containerclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(read_container.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

read_containerclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library read_container
	-$(cleanup_silent) cd $(bin) && \rm -f $(library_prefix)read_container$(library_suffix).a $(library_prefix)read_container$(library_suffix).$(shlibsuffix) read_container.stamp read_container.shstamp
#-- end of cleanup_library ---------------

# echo "cleanup read_container read_container-00-00-00 in /data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.7.5/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtread_containertempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtread_containertempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=read_container -version=read_container-00-00-00 -path=/data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf  $* >${cmtread_containertempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=read_container -version=read_container-00-00-00 -path=/data/olariu/NCB/Simulation/ATLMCPROD-4508-Cosmic-rays/rel19/Reco_tf  $* >${cmtread_containertempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtread_containertempfile}
  unset cmtread_containertempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtread_containertempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtread_containertempfile}
unset cmtread_containertempfile
exit $cmtcleanupstatus


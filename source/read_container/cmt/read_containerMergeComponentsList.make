#-- start of make_header -----------------

#====================================
#  Document read_containerMergeComponentsList
#
#   Generated Tue Apr 11 18:10:20 2017  by olariu
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_read_containerMergeComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_read_containerMergeComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_read_containerMergeComponentsList

read_container_tag = $(tag)

#cmt_local_tagfile_read_containerMergeComponentsList = $(read_container_tag)_read_containerMergeComponentsList.make
cmt_local_tagfile_read_containerMergeComponentsList = $(bin)$(read_container_tag)_read_containerMergeComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

read_container_tag = $(tag)

#cmt_local_tagfile_read_containerMergeComponentsList = $(read_container_tag).make
cmt_local_tagfile_read_containerMergeComponentsList = $(bin)$(read_container_tag).make

endif

include $(cmt_local_tagfile_read_containerMergeComponentsList)
#-include $(cmt_local_tagfile_read_containerMergeComponentsList)

ifdef cmt_read_containerMergeComponentsList_has_target_tag

cmt_final_setup_read_containerMergeComponentsList = $(bin)setup_read_containerMergeComponentsList.make
cmt_dependencies_in_read_containerMergeComponentsList = $(bin)dependencies_read_containerMergeComponentsList.in
#cmt_final_setup_read_containerMergeComponentsList = $(bin)read_container_read_containerMergeComponentsListsetup.make
cmt_local_read_containerMergeComponentsList_makefile = $(bin)read_containerMergeComponentsList.make

else

cmt_final_setup_read_containerMergeComponentsList = $(bin)setup.make
cmt_dependencies_in_read_containerMergeComponentsList = $(bin)dependencies.in
#cmt_final_setup_read_containerMergeComponentsList = $(bin)read_containersetup.make
cmt_local_read_containerMergeComponentsList_makefile = $(bin)read_containerMergeComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)read_containersetup.make

#read_containerMergeComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'read_containerMergeComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = read_containerMergeComponentsList/
#read_containerMergeComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_componentslist_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.components file into a single
# <project>.components file in the (lib) install area
# If no InstallArea is present the fragment is dummy


.PHONY: read_containerMergeComponentsList read_containerMergeComponentsListclean

# default is already '#'
#genmap_comment_char := "'#'"

componentsListRef    := ../$(tag)/read_container.components

ifdef CMTINSTALLAREA
componentsListDir    := ${CMTINSTALLAREA}/$(tag)/lib
mergedComponentsList := $(componentsListDir)/$(project).components
stampComponentsList  := $(componentsListRef).stamp
else
componentsListDir    := ../$(tag)
mergedComponentsList :=
stampComponentsList  :=
endif

read_containerMergeComponentsList :: $(stampComponentsList) $(mergedComponentsList)
	@:

.NOTPARALLEL : $(stampComponentsList) $(mergedComponentsList)

$(stampComponentsList) $(mergedComponentsList) :: $(componentsListRef)
	@echo "Running merge_componentslist  read_containerMergeComponentsList"
	$(merge_componentslist_cmd) --do-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList)

read_containerMergeComponentsListclean ::
	$(cleanup_silent) $(merge_componentslist_cmd) --un-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList) ;
	$(cleanup_silent) $(remove_command) $(stampComponentsList)
libread_container_so_dependencies = ../x86_64-slc6-gcc49-opt/libread_container.so
#-- start of cleanup_header --------------

clean :: read_containerMergeComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(read_containerMergeComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

read_containerMergeComponentsListclean ::
#-- end of cleanup_header ---------------

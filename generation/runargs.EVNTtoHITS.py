# Run arguments file auto-generated on Fri Oct 21 12:13:52 2022 by:
# JobTransform: EVNTtoHITS
# Version: $Id: trfExe.py 792052 2017-01-13 13:36:51Z mavogel $
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'EVNTtoHITS' 

runArgs.preInclude = ['SimulationJobOptions/preInclude.Cosmics.py']
runArgs.simulator = 'FullG4'
runArgs.truthStrategy = 'MC15aPlus'
runArgs.maxEvents = 1000
runArgs.randomSeed = 1234
runArgs.DataRunNumber = 284500
runArgs.CosmicFilterVolume = 'NONE'
runArgs.CosmicFilterVolume2 = 'NONE'
runArgs.geometryVersion = 'ATLAS-R3S-2021-01-00-00_VALIDATION'
runArgs.conditionsTag = 'OFLCOND-MC16-SDR-25'
runArgs.physicsList = 'FTFP_BERT'
runArgs.postInclude = ['RecJobTransforms/UseFrontier.py']
runArgs.CosmicPtSlice = 'NONE'
runArgs.beamType = 'cosmics'

 # Input data

 # Output data
runArgs.outputEVNT_TRFile = 'cosmics.TR.pool.root'
runArgs.outputEVNT_TRFileType = 'evnt'
runArgs.outputHITSFile = 'cosmics.HITS.pool.root'
runArgs.outputHITSFileType = 'HITS'

 # Extra runargs

 # Extra runtime runargs

 # Literal runargs snippets

reco_dir=reconstruction
inputfile_HITS=../generation/cosmics.HITS.pool.root

[ -d $reco_dir ] || mkdir $reco_dir

cd $reco_dir

Reco_tf.py \
--maxEvents=-1 \
--skipEvents=0 \
--digiSteeringConf 'StandardSignalOnlyTruth' \
--conditionsTag 'OFLCOND-MC16-SDR-25' \
--autoConfiguration 'everything' \
--DataRunNumber 284500 \
--beamType 'cosmics' \
--geometryVersion 'ATLAS-R3S-2021-01-00-00' \
--numberOfCavernBkg '0' \
--ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
--postInclude 'default:RecJobTransforms/UseFrontier.py' \
--preInclude='RDOtoRDOTrigger:RecExPers/RecoOutputMetadataList_jobOptions.py,Digitization/SimulationJobOptions/RunDependentSimData/configLumi_run284500_mc16a.py,ForceUseOfPileUpTools.py' \
--postExec 'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"]' 'HITtoRDO:from AthenaCommon.AlgSequence import AlgSequence;topSequence=AlgSequence();from read_container.read_containerConf import read_HITSAlg;read_HITSAlg=read_HITSAlg();read_HITSAlg.outputFile="read_HITS.root";topSequence+=read_HITSAlg' 'from IOVDbSvc.CondDB import conddb;conddb.addOverride("/TRT/Calib/T0","TrtCalibT0-MCCosmics_00-00");conddb.addOverride("TRT/Calib/RT","TrtCalibRt-MCCosmics_00-00")' "ESDtoAOD:fixedAttrib=[s if \"CONTAINER_SPLITLEVEL = \'99\'\" not in s else \"\" for s in svcMgr.AthenaPoolCnvSvc.PoolAttributes];svcMgr.AthenaPoolCnvSvc.PoolAttributes=fixedAttrib" \
--preExec 'all:from Digitization.DigitizationFlags import jobproperties;jobproperties.Digitization.doInDetNoise.set_Value_and_Lock(False)' 'all:rec.doTrigger.set_Value_and_Lock(False);rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);jobproperties.Beam.beamType.set_Value_and_Lock("beamType");jobproperties.Beam.energy.set_Value_and_Lock(6000000);jobproperties.Beam.bunchSpacing.set_Value_and_Lock(50);from AthenaCommon.DetFlags import DetFlags;DetFlags.ID_setOn();DetFlags.Calo_setOn();DetFlags.Muon_setOn();DetFlags.BField_setOn();from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True)' 'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False;jobproperties.CaloCellFlags.doPileupOffsetBCIDCorr.set_Value_and_Lock(False);from InDetRecExample.InDetJobProperties import InDetFlags;InDetFlags.doInnerDetectorCommissioning.set_Value_and_Lock(True);InDetFlags.useBroadClusterErrors.set_Value_and_Lock(False);from JetRec.JetRecFlags import jetFlags;jetFlags.useTracks=False' \
--inputHITSFile=$inputfile_HITS \
--outputRDOFile=RDO.pool.root \
--outputRDOFile=RDO.pool.root \
--outputESDFile=ESD.pool.root \
--outputAODFile=AOD.pool.root \

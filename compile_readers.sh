readers_path_dir=testarea

[ -d $readers_path_dir ] || mkdir $readers_path_dir
cd $readers_path_dir

cmake ../source/
make
source x86_64-centos7-gcc8-opt/setup.sh

cd ../

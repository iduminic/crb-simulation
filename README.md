## Cloning the repository:

```
git clone https://gitlab.cern.ch/atlas-data-preparation/ncb.git
```

## Switch to the branch that contains the cosmics code

```
git checkout MCCosmics_summer_stud
```

## Setup the ATLAS software environment

```
cd Software/offline/MCCosmics/
source script.sh
```

## Generation/Simulation

```
source Sim_tf.sh
```

## Compile the readers

```
source compile_readers.sh
```

## Digitization + Reconstruction

```
source Reco_tf.sh
```
